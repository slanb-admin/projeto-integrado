import React, { Component } from 'react';
import { SafeAreaView, StatusBar, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Footer from './src/FrontEnd/components/Footer';
import Colors from './src/FrontEnd/styles/Colors';
import FlashMessage from 'react-native-flash-message'
import firebase from 'firebase'
import * as Font from 'expo-font';
import Login from './src/FrontEnd/screens/Login';
import Register from './src/FrontEnd/screens/Register';
import { createStackNavigator } from '@react-navigation/stack';
import Database from './src/BackEnd/db/Database';
import GlobalState from './src/Commons/GlobalState';
import Utils from './src/Commons/Utils'

const Stack = createStackNavigator();

let customFonts = {
  'Montserrat Medium': require('./assets/fonts/Montserrat-Medium.ttf'),
  'Montserrat SemiBold': require('./assets/fonts/Montserrat-SemiBold.ttf'),
  'Open Sans Bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  'Open Sans Light': require('./assets/fonts/OpenSans-Light.ttf'),
  'Open Sans Regular': require('./assets/fonts/OpenSans-Regular.ttf'),
  'Open Sans SemiBold': require('./assets/fonts/OpenSans-SemiBold.ttf'),
};

let savedCasesUnsubscriber;
let registeredCasesUnsubscriber;

export default class App extends Component {

  //#region CONSTRUCTOR, MOUNT AND UNMOUNT
  constructor(props) {
    super(props)
    this.state = {
      isUserLogged: false,
      isLoadingApp: true,
    }
  }

  async componentDidMount() {
    await Font.loadAsync(customFonts);

    firebase.auth().onAuthStateChanged(async (user) => {
      let isCurrentUserLogged = false
      if (user) {
        await this.checkIfItIsNecessaryToCreateUserTables()
        this.registerDatabaseSnapshotEvents()
        isCurrentUserLogged = true
      }
      this.setState({
        isUserLogged: isCurrentUserLogged,
        isLoadingApp: false
      });
    });
  }

  componentWillUnmount() {
    savedCasesUnsubscriber()
    registeredCasesUnsubscriber()
  }
  //#endregion

  //#region METHODS

  //#region DATABASE SNAPSHOTS
  registerDatabaseSnapshotEvents() {
    this.registerUserSavedCasesEvent()
    this.registerCasesSnapshotEvent()
  }

  registerUserSavedCasesEvent() {
    let userUid = Utils.getCurrentUserFirebaseUid()
    savedCasesUnsubscriber = firebase.firestore().collection(Database.TABLE_SAVED_CASES).doc(userUid).onSnapshot(async () => {
      console.log("<< SNAPSHOT SAVED CASES >>")
      let userSavedCases = await Database.getSavedOrRegisteredCasesIdAsync(Database.TABLE_SAVED_CASES)

      if (GlobalState.HOME_SCREEN != null) {
        GlobalState.HOME_SCREEN.state.userSavedCases = userSavedCases
      }

      if (GlobalState.SEARCH_SCREEN != null) {
        GlobalState.SEARCH_SCREEN.state.userSavedCases = userSavedCases
      }

    })
  }

  registerCasesSnapshotEvent() {
    registeredCasesUnsubscriber = firebase.firestore().collection(Database.TABLE_CASES).onSnapshot(async () => {
      console.log("<< SNAPSHOT ALL CASES >>")
      let cases = await Database.getAllRegisteredCasesAsArray()

      if (GlobalState.HOME_SCREEN != null) {
        GlobalState.HOME_SCREEN.state.cases = cases.length > 20 ? cases : cases.slice(0, 20)
      }

      if (GlobalState.SEARCH_SCREEN != null) {
        GlobalState.SEARCH_SCREEN.state.cases = cases
      }
    })
  }
  //#endregion

  async checkIfItIsNecessaryToCreateUserTables() {
    let userMetadata = firebase.auth().currentUser.metadata
    let isNewUser = userMetadata.creationTime == userMetadata.lastSignInTime
    console.log("<< IS FIRST TIME LOGGING : " + isNewUser + " >>")
    if (isNewUser) {
      Database.createUserTable(Database.TABLE_SAVED_CASES)
      Database.createUserTable(Database.TABLE_USER_REGISTERED_CASES)
      Database.createUserProfileTable()
    }
  }
  //#endregion

  //#region RENDER
  render() {
    if (this.state.isLoadingApp) {
      return null
    } else if (this.state.isUserLogged) {
      return (
        <NavigationContainer style={{ flex: 1 }} theme={{ colors: { background: Colors.BACKGROUND_COLOR } }}>
          <StatusBar barStyle="light-content" backgroundColor={Colors.BACKGROUND_COLOR} />
          <Footer />
          <FlashMessage position="bottom" />
        </NavigationContainer>
      )
    } else {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BACKGROUND_COLOR }}>
          <NavigationContainer style={{ flex: 1 }} theme={{ colors: { background: Colors.BACKGROUND_COLOR } }}>
            <StatusBar barStyle="light-content" backgroundColor={Colors.BACKGROUND_COLOR} />
            <Stack.Navigator screenOptions={{ headerShown: false }}>
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="Register" component={Register} />
            </Stack.Navigator>
            <FlashMessage position="bottom" />
          </NavigationContainer>
        </SafeAreaView>
      )
    }
  }
  //#endregion
}