import firebase from "firebase";
import Enums from "../../Commons/Enums";
import Utils from "../../Commons/Utils";

export default class Storage {

    //#region FOLDERS
    static IMAGE_FOLDER = "images/"
    static PROFILE_FOLDER = "profile/"
    static AI_FOLDER = "ai/"
    //#endregion

    //#region METHODS
    static async uploadPhotoToStorageAsync(imageUri, folderEnum) {
        let currentFolder = this.getFolderToBeUsed(folderEnum)
        const response = await fetch(imageUri)
        const blob = await response.blob()
        let filePath = Utils.getImageFileUniqueName(currentFolder)
        let ref = firebase.storage().ref().child(filePath);
        await ref.put(blob)
        let downloadURI = await firebase.storage().ref(filePath).getDownloadURL();
        return downloadURI
    }

    static async deleteImageFileFromStorage(imageURL) {
        await firebase.storage().refFromURL(imageURL).delete()
    }

    static getFolderToBeUsed(folderEnum) {
        let currentFolder;
        switch (folderEnum) {
            case Enums.CurrentStorageFolder.IMAGES_FOLDER:
                currentFolder = this.IMAGE_FOLDER;
                break;
            case Enums.CurrentStorageFolder.PROFILE_FOLDER:
                currentFolder = this.PROFILE_FOLDER;
                break;
            case Enums.CurrentStorageFolder.AI_FOLDER:
                currentFolder = this.AI_FOLDER;
                break;
        }
        return currentFolder;
    }
    //#endregion

}