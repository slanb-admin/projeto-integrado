import firebase from "firebase"
import Utils from "../../Commons/Utils"

export default class Database {

    //#region TABLES
    static TABLE_CASES = 'cases'
    static TABLE_SAVED_CASES = 'savedCases'
    static TABLE_USER_REGISTERED_CASES = 'registeredCases'
    static TABLE_PROFILE = 'profile'
    //#endregion

    //#region CASES
    static updateCaseInfos(newCase) {
        firebase.firestore().collection(this.TABLE_CASES).doc(newCase.id).update({
            name: newCase.name,
            description: newCase.description,
            birthDate: newCase.birthDate,
            disappearanceDate: newCase.disappearanceDate,
            city: newCase.city,
            gender: newCase.gender,
            image: newCase.image,
        })
    }

    static async registerCaseAsync(caseInfos) {
        let uid = Utils.getCurrentUserFirebaseUid()
        let currentCases = []

        //Add new case to database
        let newCase = await firebase.firestore().collection(this.TABLE_CASES).add({
            name: caseInfos.name,
            description: caseInfos.description,
            birthDate: caseInfos.birthDate,
            disappearanceDate: caseInfos.disappearanceDate,
            city: caseInfos.city,
            gender: caseInfos.gender,
            id: '',
            image: caseInfos.image,
        })

        //Add id field, to map cases across the app
        firebase.firestore().collection(this.TABLE_CASES).doc(newCase.id).update({
            id: newCase.id
        })

        //Get current cases and push new id to it
        await firebase.firestore().collection(this.TABLE_USER_REGISTERED_CASES).doc(uid).get().then(qs => {
            currentCases = qs.data().cases
            currentCases.push(newCase.id)
        })

        //Update user table with new array of ids
        firebase.firestore().collection(this.TABLE_USER_REGISTERED_CASES).doc(uid).update({
            cases: currentCases
        })
    }

    static async getAllRegisteredCasesAsArray() {
        let casesAndIds = []
        await firebase.firestore().collection(this.TABLE_CASES).get().then(qs => {
            qs.forEach(doc => {
                casesAndIds.push(doc.data())
            })
        })
        return casesAndIds
    }

    static async getCasesFromAIResponseAsync(aiResponse) {
        let cases = []
        for (let i = 0; i < aiResponse.length; i++) {
            await firebase.firestore().collection(this.TABLE_CASES).doc(aiResponse[i]).get().then(qs => {
                cases.push(qs.data())
            })
        }
        return cases
    }
    //#endregion

    //#region SAVED AND REGISTERED CASES
    static createUserTable(table) {
        let uid = Utils.getCurrentUserFirebaseUid()
        firebase.firestore().collection(table).doc(uid).set({
            cases: []
        })
    }

    static async deleteRegisteredCaseAsync(caseId) {
        let uid = Utils.getCurrentUserFirebaseUid()
        let currentUserCases = []
        firebase.firestore().collection(this.TABLE_CASES).doc(caseId).delete()
        await firebase.firestore().collection(this.TABLE_USER_REGISTERED_CASES).doc(uid).get().then(qs => {
            currentUserCases = qs.data().cases
            let indexOfCase = currentUserCases.indexOf(caseId)
            currentUserCases.splice(indexOfCase, 1)
        })

        firebase.firestore().collection(this.TABLE_USER_REGISTERED_CASES).doc(uid).update({
            cases: currentUserCases
        })
    }

    static async removeCaseFromSavedCasesTableAsync(caseId) {
        let uid = Utils.getCurrentUserFirebaseUid()
        let currentCases = []
        await firebase.firestore().collection(this.TABLE_SAVED_CASES).doc(uid).get().then(qs => {
            currentCases = qs.data().cases
            let indexOfCase = currentCases.indexOf(caseId)
            currentCases.splice(indexOfCase, 1)
        })
        firebase.firestore().collection(this.TABLE_SAVED_CASES).doc(uid).update({
            cases: currentCases
        })
    }

    static async saveCaseToSavedCasesTableAsync(caseId) {
        let uid = Utils.getCurrentUserFirebaseUid()
        let currentCases = []

        await firebase.firestore().collection(this.TABLE_SAVED_CASES).doc(uid).get().then(qs => {
            currentCases = qs.data().cases
            currentCases.push(caseId)
        })

        firebase.firestore().collection(this.TABLE_SAVED_CASES).doc(uid).update({
            cases: currentCases
        })
    }

    static async getSavedOrRegisteredCasesIdAsync(table) {
        let uid = Utils.getCurrentUserFirebaseUid()
        let casesIds = []
        await firebase.firestore().collection(table).doc(uid).get().then(qs => {
            casesIds = qs.data().cases
        })
        return casesIds
    }

    static async getSavedOrRegisteredCasesAsync(table) {
        let uid = Utils.getCurrentUserFirebaseUid()
        let cases = []

        //get saved cases id from saved or registered cases table
        await firebase.firestore().collection(table).doc(uid).get().then(doc => {
            cases = doc.data().cases
        })


        if (cases.length > 0) {
            let userRegisteredCases = []

            for (let i = 0; i < cases.length; i++) {
                let snapshot = await firebase.firestore().collection(this.TABLE_CASES).doc(cases[i]).get()

                if (snapshot.exists) {
                    userRegisteredCases.push(snapshot.data())
                } else {
                    cases.splice(cases.indexOf(cases[i]), 1)
                    firebase.firestore().collection(table).doc(Utils.getCurrentUserFirebaseUid()).update({
                        cases: cases
                    })
                }
            }

            return userRegisteredCases
        }
        return []
    }
    //#endregion

    //#region PROFILE
    static async getUserNameAndProfileImageAsync() {
        let userNameAndProfileImage = []
        let uid = Utils.getCurrentUserFirebaseUid()
        await firebase.firestore().collection(this.TABLE_PROFILE).doc(uid).get().then(qs => {
            let data = qs.data()
            userNameAndProfileImage.push(data.image)
            userNameAndProfileImage.push(data.name)
        })
        return userNameAndProfileImage
    }

    static createUserProfileTable() {
        let uid = Utils.getCurrentUserFirebaseUid()
        let randomUserName = 'Usuario' + Math.random().toString(36).substr(2, 5)
        firebase.firestore().collection(this.TABLE_PROFILE).doc(uid).set({
            image: '',
            name: randomUserName,
        })
    }

    static updateProfileInfos(newInfos) {
        let uid = Utils.getCurrentUserFirebaseUid()
        firebase.firestore().collection(this.TABLE_PROFILE).doc(uid).update({
            image: newInfos.image,
            name: newInfos.name,
        })
    }
    //#endregion
}