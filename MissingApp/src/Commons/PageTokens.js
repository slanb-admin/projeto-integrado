//Responsible for storing the page tokens used for navigation
export default class PageTokens {
    static CAMERA = "CameraW"
    static CREATE_CASE = "CreateCase"
    static HOME = "Home"
    static LOGIN = "Login"
    static PROFILE = "Profile"
    static REGISTER = "Register"
    static SAVED_CASES = "SavedCases"
    static SEARCH = "Search"
    static SETTINGS = "Settings"
    static USER_REGISTERED_CASES = "UserRegisteredCases"
    static EDIT_PROFILE = "EditProfile"
}