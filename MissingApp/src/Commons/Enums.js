export default class Enums {

    //Filter buttons order
    static OrderEnum = {
        NONE: 0,
        LOWER_AGE: 1,
        HIGHER_AGE: 2,
        LOWER_DATE: 3,
        HIGHER_DATE: 4,
    }

    //Search.js pages
    static ActivePageEnum = {
        SEARCH: 0,
        FILTER: 1,
        DETAILED_CASE: 2,
    }

    //BackIconHeader.js icon
    static HeaderIcon = {
        NONE: 0,
        BOOKMARK: 1,
        DELETE: 2,
    }

    //Modifier page caller
    static CurrentModifierCasePage = {
        CREATE_CASE: 0,
        USER_REGISTERED_CASE: 1,
    }

    //Used to store images on firebase storage
    static CurrentStorageFolder = {
        IMAGES_FOLDER: 0,
        PROFILE_FOLDER: 1,
        AI_FOLDER: 2,
    }
}