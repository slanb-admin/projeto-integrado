//Responsible for storing all the strings used across the application
export default class StringConstants {

    //#region FLASH MESSAGES
    static MISSING_APP_EMAIL_ALREADY_IN_USE = "Este email já está em uso"
    static MISSING_APP_INVALID_EMAIL = "Email inválido"
    static MISSING_APP_FILL_ALL_FIELDS = "Por favor preencha todos os campos"
    static MISSING_APP_CASE_CREATED = "Caso criado"
    static MISSING_APP_CASE_MODIFIED = "Caso Modificado"
    static MISSING_APP_USER_PASSWORD_INVALID = "Usuário ou senha inválidos"
    static MISSING_APP_USER_INVALID_DATE_1 = "Data inválida! Data de nascimento maior que a de desaparecimento"
    static MISSING_APP_USER_INVALID_DATE_2 = "Data inválida! Data de nascimento maior que dia atual"
    static MISSING_APP_USER_INVALID_DATE_3 = "Data inválida! Data de desaparecimento maior que dia atual"
    static MISSING_APP_INVALID_USER_NAME = "Nome de usuário inválido"
    static MISSING_APP_PROFILE_UPDATED = "Perfil atualizado"
    static MISSING_APP_SMALL_PASSWORD = "Senha precisa ter mais que 5 caracteres"
    static MISSING_APP_INVALID_PASSWORD = "Senha precisa ter pelo menos um número, uma letra maiúscula e minúscula"
    static MISSING_APP_AI_ERROR_MESSAGE = "Erro ao receber resposta da IA"
    static MISSING_APP_NO_AI_CASES_FOUND = "Nenhum caso foi encontrado"
    static MISSING_APP_MATCHING_CASE = "É a mesma pessoa"
    static MISSING_APP_NO_MATCHING_CASE = "Não é a mesma pessoa. Tente subir outra foto"
    static MISSING_APP_MATCHING_CASE_INFO = "Sua foto corresponde com a foto cadastrada"
    static MISSING_APP_NO_MATCHING_CASE_INFO = "Sua foto não corresponde com a foto cadastrada"
    //#endregion

    //#region FONTS
    static MISSING_APP_MONTSERRAT_MEDIUM_FONT = "Montserrat Medium"
    static MISSING_APP_MONTSERRAT_SEMI_BOLD_FONT = "Montserrat SemiBold"
    static MISSING_APP_OPEN_SANS_BOLD_FONT = "Open Sans Bold"
    static MISSING_APP_OPEN_SANS_LIGHT_FONT = "Open Sans Light"
    static MISSING_APP_OPEN_SANS_REGULAR_FONT = "Open Sans Regular"
    static MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT = "Open Sans SemiBold"
    //#endregion

    //#region PLACEHOLDERS
    static MISSING_APP_EMAIL_PLACEHOLDER = "Email";
    static MISSING_APP_PASSWORD_PLACEHOLDER = "Senha";
    static MISSING_APP_PERSON_NAME_PLACEHOLDER = "Nome do desaparecido"
    static MISSING_APP_PERSON_DESCRIPTION_PLACEHOLDER = "Descrição da pessoa"
    static MISSING_APP_SEARCH_PLACEHOLDER = "Pesquisar"
    static MISSING_APP_FILTER_PLACEHOLDER = "Filtragem"
    static MISSING_APP_DISAPPEARANCE_DATE = "Data do desaparecimento"
    static MISSING_APP_BIRTH_DATE = "Data de nascimento do desaparecido"
    static MISSING_APP_DISAPPEARANCE_CITY = "Cidade do desaparecimento"
    static MISSING_APP_PERSON_GENDER = "Sexo do desaparecido"
    static MISSING_APP_USER_NAME_PLACEHOLDER = "Nome de usuário"
    //#endregion

    //#region PAGES TITLE
    static MISSING_APP_DETAILED_CASE_TITLE = "Caso Detalhado"
    static MISSING_APP_SAVED_CASES_TITLE = "Casos Salvos"
    static MISSING_APP_EDIT_PROFILE_TITLE = "Editar Perfil"
    static MISSING_APP_CREATE_CASE_TITLE = "Cadastrar caso"
    static MISSING_APP_EDIT_CASE_TITLE = "Editar caso"
    static MISSING_APP_REGISTERED_CASES_TITLE = "Casos Cadastrados"
    static MISSING_APP_SETTINGS_TITLE = "Configurações"
    static MISSING_APP_AI_CASES_TITLE = "Casos encontrados"
    //#endregion

    static MISSING_APP_OK_STRING = "OK"
    static MISSING_APP_CANCEL_STRING = "Cancel"
}