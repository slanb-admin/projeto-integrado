import React from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import Icon from 'react-native-vector-icons/MaterialIcons'

import Colors from '../styles/Colors'

// Import pages
import Home from '../screens/Home'
import Search from '../screens/Search'
import Profile from '../screens/Profile'
import Camera from '../screens/CameraW'

import SavedCases from '../screens/SavedCases'
import CreateCase from '../screens/CreateCase'
import EditProfile from '../screens/EditProfile'
import UserRegisteredCases from '../screens/UserRegisteredCases'
import Settings from '../screens/Settings'

import CustomStyles from '../styles/CustomStyles'
import PageTokens from '../../Commons/PageTokens'

const Tab = createMaterialBottomTabNavigator()
const PStack = createStackNavigator()

const Footer = () => {

    function ProfileStack() {
        return (
            <PStack.Navigator screenOptions={{ headerShown: false }}>
                <PStack.Screen name={PageTokens.PROFILE} component={Profile} />
                <PStack.Screen name={PageTokens.EDIT_PROFILE} component={EditProfile} />
                <PStack.Screen name={PageTokens.CREATE_CASE} component={CreateCase} />
                <PStack.Screen name={PageTokens.USER_REGISTERED_CASES} component={UserRegisteredCases} />
                <PStack.Screen name={PageTokens.SAVED_CASES} component={SavedCases} />
                <PStack.Screen name={PageTokens.SETTINGS} component={Settings} />
            </PStack.Navigator>
        )
    }

    return (
        <Tab.Navigator inactiveColor="rgba(255, 255, 255, 8.5)" activeColor={Colors.WHITE} barStyle={CustomStyles.footerBarStyle}>
            <Tab.Screen name="Home" component={Home} options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color }) => (
                    <Icon name="home" color={color} size={24} />
                ),
            }} />
            <Tab.Screen name="Search" component={Search} options={{
                tabBarLabel: 'Pesquisar',
                tabBarIcon: ({ color }) => (
                    <Icon name="search" color={color} size={24} />
                ),
            }} />
            <Tab.Screen name="Camera" component={Camera} options={{
                tabBarLabel: 'Camera',
                tabBarIcon: ({ color }) => (
                    <Icon name="camera-alt" color={color} size={24} />
                ),
            }} />
            <Tab.Screen name="Profile" component={ProfileStack} options={{
                tabBarLabel: 'Perfil',
                tabBarIcon: ({ color }) => (
                    <Icon name="account-circle" color={color} size={24} />
                ),
            }} />
        </Tab.Navigator>
    );
}

export default Footer;