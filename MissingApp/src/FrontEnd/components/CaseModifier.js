import React, { Component } from 'react'
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    TextInput,
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import SelectDropdown from 'react-native-select-dropdown'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import Utils from '../../Commons/Utils'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Colors from '../styles/Colors'
import StringConstants from '../../Commons/StringConstants'
import GlobalState from '../../Commons/GlobalState'
import propTypes from 'prop-types'
import Enums from '../../Commons/Enums'

export default class CaseModifier extends Component {

    //#region CONSTRUCTOR
    constructor(props) {
        super(props)
        this.state = {
            name: this.props.caseInfos.name,
            description: this.props.caseInfos.description,
            birthDate: this.props.caseInfos.birthDate,
            disappearanceDate: this.props.caseInfos.disappearanceDate,
            city: this.props.caseInfos.city,
            gender: this.props.caseInfos.gender,
            imageURI: this.props.caseInfos.imageURI,
        }
    }
    //#endregion

    //#region METHODS
    pickImage = async () => {
        let result = await Utils.openImagePickerAsync()

        if (!result.cancelled) {
            this.setState({
                imageURI: result.uri
            })
        }
    }

    validateInfos = async () => {
        if (this.state.name === '' ||
            this.state.description === '' ||
            this.state.birthDate === '' ||
            this.state.disappearanceDate === '' ||
            this.state.city === '' ||
            this.state.gender === '' ||
            this.state.imageURI === '') {
            Utils.showFlashMessage(StringConstants.MISSING_APP_FILL_ALL_FIELDS)
        } else if (Utils.isValidDate(this.state.birthDate, this.state.disappearanceDate)) {
            this.setPageInfo()
            await this.props.updateCase()
        }
    }

    setPageInfo() {
        if (this.props.currentModifierCasePage === Enums.CurrentModifierCasePage.CREATE_CASE) {
            if (GlobalState.CREATE_CASE_SCREEN != null) {
                GlobalState.CREATE_CASE_SCREEN.state.name = this.state.name
                GlobalState.CREATE_CASE_SCREEN.state.description = this.state.description
                GlobalState.CREATE_CASE_SCREEN.state.birthDate = this.state.birthDate
                GlobalState.CREATE_CASE_SCREEN.state.disappearanceDate = this.state.disappearanceDate
                GlobalState.CREATE_CASE_SCREEN.state.city = this.state.city
                GlobalState.CREATE_CASE_SCREEN.state.gender = this.state.gender
                GlobalState.CREATE_CASE_SCREEN.state.imageURI = this.state.imageURI
            }
        } else {
            if (GlobalState.USER_REGISTERED_CASE_SCREEN != null) {
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.name = this.state.name
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.description = this.state.description
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.birthDate = this.state.birthDate
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.disappearanceDate = this.state.disappearanceDate
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.city = this.state.city
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.gender = this.state.gender
                GlobalState.USER_REGISTERED_CASE_SCREEN.state.imageURI = this.state.imageURI
            }
        }
    }
    //#endregion

    //#region RENDER
    render() {
        return (
            <ScrollView>
                <View style={GenericStyles.casesMainContainerStyle}>

                    <View style={GenericStyles.baseInputStyle}>
                        <TextInput placeholder={StringConstants.MISSING_APP_PERSON_NAME_PLACEHOLDER}
                            placeholderTextColor={Colors.LIGHT_WHITE}
                            value={this.state.name}
                            onChangeText={value => this.setState({ name: value })}
                            multiline={true}
                            style={[GenericStyles.baseTextStyle, CustomStyles.createCaseTextInputStyle]} />
                    </View>

                    <View style={GenericStyles.baseInputStyle}>
                        <TextInput placeholder={StringConstants.MISSING_APP_PERSON_DESCRIPTION_PLACEHOLDER}
                            placeholderTextColor={Colors.LIGHT_WHITE}
                            value={this.state.description}
                            onChangeText={value => this.setState({ description: value })}
                            multiline={true}
                            style={[GenericStyles.baseTextStyle, CustomStyles.createCaseTextInputStyle]} />
                    </View>

                    <SelectDropdown
                        data={Utils.Cities}
                        defaultValueByIndex={Utils.Cities.indexOf(this.state.city)}
                        onSelect={(value) => this.setState({ city: value })}
                        defaultButtonText={StringConstants.MISSING_APP_DISAPPEARANCE_CITY}
                        buttonStyle={[GenericStyles.baseInputStyle, GenericStyles.dropdownButtonStyle]}
                        buttonTextStyle={[GenericStyles.baseTextStyle, CustomStyles.dropdownButtonTextStyle,
                        { color: this.state.city == '' ? Colors.LIGHT_WHITE : Colors.WHITE }]}
                        dropdownIconPosition="right"
                        renderDropdownIcon={() => <Icon name="arrow-drop-down" color={Colors.BUTTON_COLOR} size={20} />}
                        rowTextStyle={GenericStyles.dropdownRowTextStyle}
                    />

                    <SelectDropdown
                        data={Utils.Genders}
                        defaultValueByIndex={Utils.GendersValues.indexOf(this.state.gender)}
                        onSelect={(value) => this.setState({ gender: value[0] })}
                        defaultButtonText={StringConstants.MISSING_APP_PERSON_GENDER}
                        buttonStyle={[GenericStyles.baseInputStyle, GenericStyles.dropdownButtonStyle]}
                        buttonTextStyle={[GenericStyles.baseTextStyle, CustomStyles.dropdownButtonTextStyle,
                        { color: this.state.gender == '' ? Colors.LIGHT_WHITE : Colors.WHITE }]}
                        dropdownIconPosition="right"
                        renderDropdownIcon={() => <Icon name="arrow-drop-down" color={Colors.BUTTON_COLOR} size={20} />}
                        rowTextStyle={GenericStyles.dropdownRowTextStyle}
                    />

                    <DatePicker
                        style={GenericStyles.datePickerStyle}
                        mode="date"
                        date={this.state.birthDate}
                        format="DD/MM/YYYY"
                        minDate="01/01/1900"
                        maxDate="31/12/2099"
                        placeholder={StringConstants.MISSING_APP_BIRTH_DATE}
                        confirmBtnText={StringConstants.MISSING_APP_OK_STRING}
                        cancelBtnText={StringConstants.MISSING_APP_CANCEL_STRING}
                        showIcon={false}
                        onDateChange={(date) => this.setState({ birthDate: date })}
                        customStyles={{
                            dateInput: [GenericStyles.baseInputStyle, GenericStyles.datePickerInputStyle],
                            dateText: [GenericStyles.baseTextStyle, CustomStyles.datePickerTextAndPlaceholderStyle],
                            placeholderText: [GenericStyles.baseTextStyle, CustomStyles.datePickerTextAndPlaceholderStyle,
                            { color: this.state.birthDate === '' ? Colors.LIGHT_WHITE : Colors.WHITE }],
                            dateTouchBody: GenericStyles.datePickerTouchBodyStyle,
                        }}
                    />

                    <DatePicker
                        style={GenericStyles.datePickerStyle}
                        mode="date"
                        date={this.state.disappearanceDate}
                        format="DD/MM/YYYY"
                        minDate="01/01/1900"
                        maxDate="31/12/2099"
                        placeholder={StringConstants.MISSING_APP_DISAPPEARANCE_DATE}
                        confirmBtnText={StringConstants.MISSING_APP_OK_STRING}
                        cancelBtnText={StringConstants.MISSING_APP_CANCEL_STRING}
                        showIcon={false}
                        onDateChange={(date) => this.setState({ disappearanceDate: date })}
                        customStyles={{
                            dateInput: [GenericStyles.baseInputStyle, GenericStyles.datePickerInputStyle],
                            dateText: [GenericStyles.baseTextStyle, CustomStyles.datePickerTextAndPlaceholderStyle],
                            placeholderText: [GenericStyles.baseTextStyle, CustomStyles.datePickerTextAndPlaceholderStyle,
                            { color: this.state.disappearanceDate === '' ? Colors.LIGHT_WHITE : Colors.WHITE }],
                            dateTouchBody: GenericStyles.datePickerTouchBodyStyle,
                        }} />


                </View>

                {this.state.imageURI === '' &&
                    <TouchableOpacity activeOpacity={0.8} onPress={this.pickImage}>
                        <View style={[GenericStyles.baseButtonStyle, CustomStyles.createCasePickImageButtonStyle]}>
                            <Text style={GenericStyles.baseButtonTextStyle}>Escolher foto</Text>
                        </View>
                    </TouchableOpacity>
                }

                {this.state.imageURI !== '' &&
                    <View>
                        <Image source={{ uri: this.state.imageURI }} style={CustomStyles.createCaseImageStyle} />
                        <TouchableOpacity activeOpacity={0.8} onPress={this.pickImage}>
                            <View style={[GenericStyles.baseButtonStyle, CustomStyles.createCasePickImageButtonStyle]}>
                                <Text style={GenericStyles.baseButtonTextStyle}>Trocar foto</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                }

                <TouchableOpacity activeOpacity={0.8} onPress={this.validateInfos}>
                    <View style={GenericStyles.baseButtonStyle}>
                        <Text style={GenericStyles.baseButtonTextStyle}>{this.props.currentModifierCasePage === Enums.CurrentModifierCasePage.CREATE_CASE ? "Cadastrar Caso" : "Modificar Caso"}</Text>
                    </View>
                </TouchableOpacity>

            </ScrollView>
        )
    }
    //#endregion

}

CaseModifier.propTypes = {
    updateCase: propTypes.func.isRequired,
    caseInfos: propTypes.object.isRequired,
    currentModifierCasePage: propTypes.number.isRequired,
}