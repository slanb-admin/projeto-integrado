import React from 'react'
import { View, Image } from 'react-native'
import { Text } from 'react-native'
const CustomStyles = require('../styles/CustomStyles');

const Header = () => {
    return (
        <View style={CustomStyles.headerMainViewStyle}>
            <Text style={CustomStyles.headerTextStyle}>Missing</Text>
            <Image source={require('../../../assets/img/logo.png')} style={CustomStyles.headerLogoStyle} />
            <View style={CustomStyles.headerRectangleSeparatorStyle}></View>
        </View>
    )
}

export { Header };