import React, { Component } from 'react'
import { View, Image, Text, Modal, SafeAreaView, Alert } from 'react-native'
import BackIconHeader from './BackIconHeader'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Utils from '../../Commons/Utils'
import Colors from '../styles/Colors'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import propTypes from 'prop-types'
import StringConstants from '../../Commons/StringConstants'
import Enums from '../../Commons/Enums'
import { TouchableOpacity } from 'react-native'
import axios from 'axios'
import Storage from '../../BackEnd/storage/Storage'
import * as Progress from 'react-native-progress'

const SERVER_URL = 'http://18.228.199.209/specificcmp'

export default class DetailedCase extends Component {

    //#region CONSTRUCTOR
    constructor(props) {
        super(props)
        this.state = {
            imageURI: '',
            isProgressRingVisible: false,
        }
    }
    //#endregion

    //#region METHODS
    pickImageAsync = async () => {
        let result = await Utils.openImagePickerAsync()

        if (!result.cancelled) {
            this.setState({
                imageURI: result.uri,
                isProgressRingVisible: true,
            })

            try {
                let downloadURI = await Storage.uploadPhotoToStorageAsync(result.uri, Enums.CurrentStorageFolder.AI_FOLDER);
                var FormData = require('form-data')
                var data = new FormData();
                data.append('file', downloadURI)
                data.append('id', this.props.item.id)
                let aiResponse = await axios({
                    method: 'post',
                    url: SERVER_URL,
                    data: data,
                })

                Storage.deleteImageFileFromStorage(downloadURI)

                this.setState({
                    isProgressRingVisible: false,
                })

                if (aiResponse.data) {
                    Alert.alert(
                        StringConstants.MISSING_APP_MATCHING_CASE,
                        StringConstants.MISSING_APP_MATCHING_CASE_INFO,
                        [
                            {
                                text: StringConstants.MISSING_APP_OK_STRING,
                                style: "cancel",
                            },
                        ],
                        {
                            cancelable: true,
                        }
                    );
                } else {
                    Alert.alert(
                        StringConstants.MISSING_APP_NO_MATCHING_CASE,
                        StringConstants.MISSING_APP_NO_MATCHING_CASE_INFO,
                        [
                            {
                                text: StringConstants.MISSING_APP_OK_STRING,
                                style: "cancel",
                            },
                        ],
                        {
                            cancelable: true,
                        }
                    );
                }
            } catch (err) {
                Utils.showFlashMessage(StringConstants.MISSING_APP_AI_ERROR_MESSAGE)
            }
        }
    }
    //#endregion

    //#region RENDER
    render() {
        return (
            <Modal transparent={false} visible={true} onRequestClose={this.props.onBackPress}>
                <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>
                    {this.state.isProgressRingVisible &&
                        <View style={GenericStyles.progressCircleStyle}>
                            <Progress.Circle color={Colors.BUTTON_COLOR} size={Utils.getScreenWidth() / 2} indeterminate={true} />
                        </View>
                    }

                    {!this.state.isProgressRingVisible &&
                        <View>
                            <BackIconHeader title={StringConstants.MISSING_APP_DETAILED_CASE_TITLE}
                                onBackPress={this.props.onBackPress}
                                icon={Enums.HeaderIcon.BOOKMARK}
                                isCaseSaved={this.props.isCaseSaved}
                                caseId={this.props.caseId}
                                page={this.props.page} />

                            <View style={CustomStyles.casePhotoAndInfosContainerStyle}>
                                <View style={CustomStyles.caseImageContainerStyle}>
                                    <Image source={{ uri: this.props.item.image }} style={CustomStyles.casePersonImageStyle} />
                                </View>
                                <View style={CustomStyles.caseNameAndAgeContainerStyle}>
                                    <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseNameAndAgeTextStyle]}>{this.props.item.name}</Text>
                                    <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseNameAndAgeTextStyle]}>{Utils.getCurrentAge(this.props.item.birthDate)} anos</Text>
                                </View>
                            </View>

                            <View style={CustomStyles.casePersonInfosContainerStyle}>
                                <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseInfosTextStyle]}>Data de nascimento: {this.props.item.birthDate}</Text>
                                <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseInfosTextStyle]}>Cidade de desaparecimento: {this.props.item.city}</Text>
                                <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseInfosTextStyle]}>Data do desaparecimento: {this.props.item.disappearanceDate}</Text>
                                <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseInfosTextStyle]}>{this.props.item.description}</Text>
                            </View>


                            <TouchableOpacity activeOpacity={0.8} onPress={this.pickImageAsync}>
                                <View style={[GenericStyles.baseButtonStyle, CustomStyles.caseSimulateButton]}>
                                    <Icon name="camera-alt" size={Utils.getScreenHeight() / 22} color={Colors.WHITE} style={{ marginRight: 10 }} />
                                    <Text style={[GenericStyles.baseTextStyle, CustomStyles.caseTitleStyle]}>Acha que viu essa pessoa? Suba uma foto</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                </SafeAreaView>
            </Modal>
        )
    }
    //#endregion
}

DetailedCase.propTypes = {
    item: propTypes.object.isRequired,
    onBackPress: propTypes.func.isRequired,
    caseId: propTypes.string.isRequired,
    isCaseSaved: propTypes.bool.isRequired,
    page: propTypes.string,
}