import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import CustomStyles from '../styles/CustomStyles'
import Colors from '../styles/Colors';
import propTypes from 'prop-types'
import Database from '../../BackEnd/db/Database';
import Enums from '../../Commons/Enums';
import PageTokens from '../../Commons/PageTokens';
import Storage from '../../BackEnd/storage/Storage';

export default class BackIconHeader extends Component {

    //#region CONSTRUCTOR
    constructor(props) {
        super(props)
        this.state = {
            isCaseSaved: props.isCaseSaved
        }
    }
    //#endregion

    //#region RENDER
    render() {
        if (this.props.icon == Enums.HeaderIcon.BOOKMARK) {
            return (
                <View style={CustomStyles.backIconHeaderMainContainerStyle}>
                    <View style={CustomStyles.backIconTitleContainer}>
                        <TouchableOpacity activeOpacity={0.5}>
                            <Icon name="arrow-back" color={Colors.WHITE} size={30} onPress={this.props.onBackPress} />
                        </TouchableOpacity>
                        <Text style={CustomStyles.backIconHeaderTitleStyle}>{this.props.title}</Text>
                    </View>
                    <View style={CustomStyles.backIconIconContainer}>
                        <Icon name={this.state.isCaseSaved ? "bookmark" : "bookmark-outline"}
                            color={Colors.WHITE} size={30}
                            onPress={async () => {
                                let previousState = this.state.isCaseSaved

                                this.setState({
                                    isCaseSaved: !previousState
                                })

                                if (previousState) {
                                    console.log("<< REMOVING SAVED CASE FROM DB >>")
                                    await Database.removeCaseFromSavedCasesTableAsync(this.props.caseId)
                                    if (this.props.page === PageTokens.SAVED_CASES) {
                                        this.props.onBackPress()
                                    }
                                } else {
                                    console.log("<< ADDING SAVED CASE TO DB >>")
                                    await Database.saveCaseToSavedCasesTableAsync(this.props.caseId)
                                }
                            }} />
                    </View>
                </View>
            )
        } else if (this.props.icon == Enums.HeaderIcon.DELETE) {
            return (
                <View style={CustomStyles.backIconHeaderMainContainerStyle}>
                    <View style={CustomStyles.backIconTitleContainer}>
                        <TouchableOpacity activeOpacity={0.5}>
                            <Icon name="arrow-back" color={Colors.WHITE} size={30} onPress={this.props.onBackPress} />
                        </TouchableOpacity>
                        <Text style={CustomStyles.backIconHeaderTitleStyle}>{this.props.title}</Text>
                    </View>
                    <View style={CustomStyles.backIconIconContainer}>
                        <Icon name="delete" color={Colors.WHITE} size={30}
                            onPress={async () => {
                                await Database.deleteRegisteredCaseAsync(this.props.caseId)
                                await Storage.deleteImageFileFromStorage(this.props.imageURI)
                            }} />
                    </View>
                </View>
            )
        } else {
            return (
                <View style={CustomStyles.backIconHeaderMainContainerStyle}>
                    <TouchableOpacity activeOpacity={0.5}>
                        <Icon name="arrow-back" color={Colors.WHITE} size={30} onPress={this.props.onBackPress} />
                    </TouchableOpacity>
                    <Text style={CustomStyles.backIconHeaderTitleStyle}>{this.props.title}</Text>
                </View>
            )
        }
    }
    //#endregion

}

BackIconHeader.propTypes = {
    title: propTypes.string.isRequired,
    onBackPress: propTypes.func.isRequired,
    icon: propTypes.number.isRequired,
    isCaseSaved: propTypes.bool,
    caseId: propTypes.string,
    imageURI: propTypes.string,
    page: propTypes.string,
}