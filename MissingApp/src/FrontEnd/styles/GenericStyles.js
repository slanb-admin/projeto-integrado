'use strict'
import { StyleSheet } from 'react-native'
import Utils from '../../Commons/Utils'
import Colors from './Colors'
import StringConstants from '../../Commons/StringConstants'

module.exports = StyleSheet.create({

    //#region MAIN PAGE CONTAINER
    mainViewStyleIgnoringFooterHeight: {
        height: Utils.getScreenHeight() - 50,
    },
    //#endregion

    //#region CASES FLAT LIST
    mainFlatListContainerStyle: {
        marginLeft: 30,
        marginTop: 20,
    },

    mainCasesViewContainerStyle: {
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },

    caseInfosContainerStyle: {
        marginStart: 10,
        flexDirection: 'column',
        justifyContent: 'center',
    },

    caseNameStyle: {
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
        maxWidth: Utils.getScreenWidth() / 2,
    },

    caseAgeAndCityStyle: {
        opacity: 0.6,
        maxWidth: Utils.getScreenWidth() / 2,
    },

    casesInfosAndIconContainerStyle: {
        //width: Utils.getScreenWidth() - (Utils.getScreenWidth() / 15) - (Utils.getScreenWidth() / 5) - 40,
        justifyContent: 'space-between',
        flexDirection: 'row',
        flex: 1,
        marginRight: 20,
    },
    //#endregion

    //#region CREATE AND EDIT CASE
    casesMainContainerStyle: {
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        width: Utils.getScreenWidth() / 1.1,
        marginTop: Utils.getScreenHeight() / 30,
        flex: 1,
    },
    //#endregion

    //#region DATE PICKER
    datePickerStyle: {
        width: Utils.getScreenWidth() / 1.2,
    },

    datePickerInputStyle: {
        backgroundColor: 'transparent',
        alignSelf: 'center',
        maxWidth: Utils.getScreenWidth() / 1.2,
    },

    datePickerTouchBodyStyle: {
        height: Utils.getScreenHeight() / 18,
        marginBottom: 10,
        borderRadius: 10,
        backgroundColor: Colors.INPUT_BACKGROUND_COLOR,
    },
    //#endregion

    //#region SELECT DROPDOWN
    dropdownRowTextStyle: {
        fontSize: 16,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_REGULAR_FONT,
        color: Colors.BACKGROUND_COLOR,
    },

    dropdownButtonStyle: {
        height: Utils.getScreenHeight() / 18,
    },
    //#endregion

    //#region PROGRESS CIRCLE
    progressCircleStyle: {
        alignSelf: 'center',
        position: 'absolute',
        marginTop: Utils.getScreenHeight() / 3,
        zIndex: 1000,
    },
    //#endregion

    //#region LOGIN AND REGISTER
    mainEmailAndPasswordContainerStyle: {
        marginTop: Utils.getScreenHeight() / 2,
        position: 'absolute',
        alignSelf: 'center',
        width: Utils.getScreenWidth() / 1.2,
    },
    //#endregion

    //#region BASE COMPONENTS
    baseInputStyle: {
        backgroundColor: Colors.INPUT_BACKGROUND_COLOR,
        minHeight: Utils.getScreenHeight() / 18,
        width: Utils.getScreenWidth() / 1.2,
        borderRadius: 10,
        marginBottom: 10,
        justifyContent: 'center',
        borderWidth: 0,
        borderColor: 'transparent',
    },

    baseButtonStyle: {
        backgroundColor: Colors.BUTTON_COLOR,
        borderRadius: 10,
        width: Utils.getScreenWidth() / 1.2,
        height: Utils.getScreenHeight() / 18,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },

    baseCaseImageOnListStyle: {
        height: Utils.getScreenWidth() / 5,
        width: Utils.getScreenWidth() / 5,
        borderRadius: 10,
    },

    baseMainPageContainerStyle: {
        backgroundColor: Colors.BACKGROUND_COLOR,
        height: Utils.getScreenHeight(),
        width: Utils.getScreenWidth(),
        flex: 1,
    },

    baseTextStyle: {
        color: Colors.WHITE,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_LIGHT_FONT,
        fontSize: Utils.isSmallScreen() ? 14 : 15,
    },

    baseButtonTextStyle: {
        color: Colors.WHITE,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_BOLD_FONT,
        fontSize: Utils.isSmallScreen() ? 16 : 20,
    },
    //#endregion
});