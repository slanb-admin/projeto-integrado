
import { StyleSheet } from 'react-native'
import Utils from '../../Commons/Utils'
import Colors from './Colors'
import StringConstants from '../../Commons/StringConstants'

module.exports = StyleSheet.create({

    //#region HEADER
    headerMainViewStyle: {
        height: Utils.getScreenHeight() / 10,
        width: Utils.getScreenWidth(),
        backgroundColor: Colors.BACKGROUND_COLOR,
        alignItems: 'center',
    },

    headerTextStyle: {
        color: Colors.WHITE,
        fontFamily: StringConstants.MISSING_APP_MONTSERRAT_MEDIUM_FONT,
        fontSize: 18,
        marginTop: 3,
    },

    headerRectangleSeparatorStyle: {
        height: 2,
        backgroundColor: Colors.BUTTON_COLOR,
        width: Utils.getScreenWidth(),
        position: 'absolute',
        marginTop: (Utils.getScreenHeight() / 10) / 2,
    },

    headerLogoStyle: {
        height: Utils.getScreenHeight() / 10,
        width: Utils.getScreenHeight() / 10,
        borderRadius: 60,
        alignSelf: 'flex-start',
        position: 'absolute',
        zIndex: 1,
        marginLeft: 10,
    },
    //#endregion

    //#region BACK ICON HEADER
    backIconHeaderMainContainerStyle: {
        flexDirection: 'row',
        backgroundColor: Colors.BACKGROUND_COLOR,
        marginTop: 10,
        marginLeft: 20,
        alignItems: 'center',
    },

    backIconHeaderTitleStyle: {
        color: Colors.WHITE,
        fontFamily: StringConstants.MISSING_APP_MONTSERRAT_SEMI_BOLD_FONT,
        fontSize: Utils.isSmallScreen() ? 18 : 20,
        marginLeft: 10,
        justifyContent: 'center',
        alignSelf: 'center',
    },

    backIconTitleContainer: {
        width: Utils.getScreenWidth() / 2,
        flexDirection: 'row',
        marginTop: 10,
    },

    backIconIconContainer: {
        width: Utils.getScreenWidth() / 2 - 30,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    //#endregion

    //#region FOOTER
    footerBarStyle: {
        backgroundColor: Colors.FOOTER_COLOR,
        height: 50,
    },
    //#endregion

    //#region HOME
    homeMainViewCasesStyle: {
        backgroundColor: Colors.BUTTON_COLOR,
        width: Utils.getScreenWidth() / 1.2,
        height: Utils.getScreenWidth() / 0.9,
        borderRadius: 30,
        marginBottom: 15,
        alignSelf: 'center',
        alignItems: 'center',
    },

    homeImageAndInfoContainerStyle: {
        marginTop: 10,
    },

    homeImageStyle: {
        width: (Utils.getScreenWidth() / 1.2) - 20,
        height: (Utils.getScreenWidth() / 1.1) - 10,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
    },

    homeTextContainerStyle: {
        backgroundColor: Colors.BACKGROUND_COLOR,
        height: (Utils.getScreenWidth() / 4.95) - 10,
        width: (Utils.getScreenWidth() / 1.2) - 20,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },

    homePersonInfoDescriptionStyle: {
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
    },

    homeMoreInfoText: {
        justifyContent: 'center',
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 3,
    },
    //#endregion    

    //#region LOGIN
    loginSeparatorStyle: {
        backgroundColor: Colors.BUTTON_COLOR,
        height: 1,
        width: Utils.getScreenWidth() / 1.15,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: Utils.getScreenHeight() / 1.2,
    },

    loginTextContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute',
        marginTop: Utils.getScreenHeight() / 1.18,
    },
    //#endregion

    //#region REGISTER
    registerImageContainerStyle: {
        alignItems: 'center',
    },

    registerImageStyle: {
        width: Utils.getScreenWidth() / 2,
        height: Utils.getScreenWidth() / 2,
    },

    registerTextStyle: {
        textAlign: 'center',
        color: Colors.WHITE,
        fontSize: 25,
        fontFamily: StringConstants.MISSING_APP_MONTSERRAT_SEMI_BOLD_FONT,
    },

    registerInputStyle: {
        marginLeft: 10,
    },
    //#endregion

    //#region SEARCH AND FILTER

    //#region FILTER
    filterContainerOrderStyle: {
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 10,
    },

    filterButtonContainerStyle: {
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 10,
        width: Utils.getScreenWidth() / 1.2,
    },

    filterButtonStyle: {
        width: Utils.getScreenWidth() / 2.5,
        marginHorizontal: 10,
    },

    filterTextContainerStyle: {
        marginLeft: 30,
        marginTop: Utils.getScreenHeight() / 30,
        alignItems: 'flex-start',
    },

    filterTextButtonStyle: {
        fontSize: 13,
        textAlign: 'center',
    },

    filterContainerTextInput: {
        marginTop: 22,
        width: Utils.getScreenWidth() / 1.2,
        alignItems: 'center',
        alignSelf: 'center',
    },
    //#endregion

    //#region SEARCH
    searchButtonSearchBarInputStyle: {
        color: Colors.WHITE,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_REGULAR_FONT,
    },

    searchButtonSearchBarContainerInputStyle: {
        borderRadius: 10,
        backgroundColor: Colors.SEARCHBAR_COLOR
    },

    searchButtonSearchBarContainerStyle: {
        backgroundColor: Colors.BACKGROUND_COLOR,
        width: Utils.getScreenWidth() / 1.1,
        alignSelf: 'center',
    },

    searchFilterButtonContainerStyle: {
        flexDirection: 'row',
        marginLeft: 30,
        marginBottom: 10,
    },

    searchFilterButtonTextStyle: {
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
        marginLeft: 5,
    },
    //#endregion SEARCH

    //#endregion

    //#region DETAILED CASE
    casePhotoAndInfosContainerStyle: {
        flexDirection: 'row',
        marginLeft: 20,
        maxWidth: Utils.getScreenWidth() - 20,
        marginTop: 20,
        marginBottom: 10,
    },

    caseImageContainerStyle: {
        backgroundColor: Colors.BUTTON_COLOR,
        borderRadius: 30,
        height: Utils.getScreenWidth() / 3,
        width: Utils.getScreenWidth() / 3,
        alignItems: 'center',
        justifyContent: 'center',
    },

    casePersonImageStyle: {
        height: Utils.getScreenWidth() / 3.3,
        width: Utils.getScreenWidth() / 3.3,
        borderRadius: 30,
    },

    caseNameAndAgeContainerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
    },

    caseNameAndAgeTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'center',
    },

    casePersonInfosContainerStyle: {
        marginLeft: 20,
        marginTop: 10,
        marginBottom: 10,
        width: Utils.getScreenWidth() / 1.1,
        alignItems: 'flex-start',
    },

    caseInfosTextStyle: {
        marginBottom: 10,
        textAlign: 'left',
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_REGULAR_FONT,
    },

    caseSimulateButton: {
        flexDirection: 'row',
        width: Utils.getScreenWidth() / 1.05,
        height: Utils.getScreenHeight() / 12,
        backgroundColor: Colors.INPUT_BACKGROUND_COLOR,
    },

    caseTitleStyle: {
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
    },
    //#endregion

    //#region CREATE CASE
    createCaseTextInputStyle: {
        marginLeft: 15,
    },

    createCasePickImageButtonStyle: {
        marginBottom: 10,
        marginTop: 15,
    },

    createCaseImageStyle: {
        width: Utils.getScreenWidth() / 3,
        height: Utils.getScreenWidth() / 3,
        alignSelf: 'center',
        marginTop: 15,
        borderRadius: 10,
    },
    //#endregion

    //#region PROFILE
    profileImageContainerStyle: {
        alignSelf: 'center',
        backgroundColor: Colors.BACKGROUND_COLOR,
    },

    profileButtonContainer: {
        alignSelf: 'center',
        width: Utils.getScreenWidth() / 1.2,
        position: 'absolute',
        marginTop: (Utils.getScreenHeight() - (Utils.getScreenHeight() / 5)) / 2,
    },

    profileMainTouchableContainerStyle: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        marginBottom: 10,
        height: Utils.getScreenHeight() / 20,
    },

    profileTextButtonStyle: {
        color: Colors.BACKGROUND_COLOR,
        marginLeft: 5,
        fontSize: 15,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
    },

    profileUserNameTextStyle: {
        marginTop: 10,
        fontSize: Utils.isSmallScreen() ? 18 : 20,
        alignSelf: 'center',
    },

    profileUserImageStyle: {
        width: Utils.getScreenWidth() / 3,
        height: Utils.getScreenWidth() / 3,
        borderRadius: 90,
    },
    //#endregion

    //#region CAMERA

    cameraContainerStyle: {
        flex: 1,
        justifyContent: 'center',
    },

    cameraChangeContainerStyle: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'row',
    },

    cameraChangeIconStyle: {
        position: 'absolute',
        bottom: '5%',
        left: '5%',
    },

    cameraTakePictureButtonStyle: {
        width: Utils.getScreenWidth(),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.BACKGROUND_COLOR,
        height: Utils.getScreenHeight() / 10,
    },

    pictureTakenStyle: {
        width: Utils.getScreenWidth(),
        height: Utils.getScreenHeight() / 1.2,
    },

    pictureTakenContainerStyle: {
        flex: 1,
        backgroundColor: Colors.BACKGROUND_COLOR,
    },

    pictureTakenButtonStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        height: Utils.getScreenHeight() / 10,
    },

    //#endregion

    //#region SETTINGS
    settingsNotificationContainerStyle: {
        marginTop: Utils.getScreenHeight() / 30,
        marginLeft: 20,
        flexDirection: 'row',
        marginBottom: 10,
    },

    settingsTextStyle: {
        color: Colors.WHITE,
        fontSize: 16,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_BOLD_FONT,
        marginLeft: 5,
    },

    settingsThemeContainerStyle: {
        marginLeft: 20,
        marginBottom: 10,
        flexDirection: 'row',
    },

    settingsRadioButtonContainerStyle: {
        flexDirection: 'row',
        marginLeft: Utils.getScreenWidth() / 8,
        justifyContent: 'space-between',
        width: Utils.getScreenWidth() / 2,
    },

    settingsRadioButtonTextStyle: {
        color: Colors.WHITE,
        fontSize: 16,
        fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
        marginRight: 20,
    },
    //#endregion

    //#region DROPDOWN
    dropdownButtonTextStyle: {
        textAlign: 'left',
    },
    //#endregion

    //#region DATE PICKER
    datePickerTextAndPlaceholderStyle: {
        alignSelf: 'flex-start',
        marginLeft: 15,
        marginTop: 5,
    },
    //#endregion

});