import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    FlatList,
    BackHandler,
    Modal,
    SafeAreaView,
} from 'react-native'
import BackIconHeader from '../components/BackIconHeader'
import firebase from 'firebase'
import Utils from '../../Commons/Utils'
import Colors from '../styles/Colors'
import GenericStyles from '../styles/GenericStyles'
import Database from '../../BackEnd/db/Database'
import StringConstants from '../../Commons/StringConstants'
import Enums from '../../Commons/Enums'
import * as Progress from 'react-native-progress'
import CaseModifier from '../components/CaseModifier'
import GlobalState from '../../Commons/GlobalState'
import Storage from '../../BackEnd/storage/Storage'

let unsubscriber;

export default class UserRegisterCases extends Component {

    //#region CONSTRUCTOR, MOUNT AND UNMOUNT
    constructor(props) {
        super(props)
        this.state = {
            cases: [],
            isEditingCase: false,
            caseEditing: [],
            name: '',
            description: '',
            birthDate: '',
            disappearanceDate: '',
            city: '',
            gender: '',
            imageURI: '',
            isProgressRingVisible: false,
        }
        GlobalState.USER_REGISTERED_CASE_SCREEN = this
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)

        this.registerUserCasesSnapshotEvent()

        try {
            this.updateUserRegisteredCases()
        } catch (err) {
            console.log("<< ERROR FETCHING DATA FROM DATABASE -> USER REGISTERED CASES PAGE >>")
            console.log(err)
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
        unsubscriber()
    }
    //#endregion

    //#region METHODS
    registerUserCasesSnapshotEvent() {
        let uid = Utils.getCurrentUserFirebaseUid()
        unsubscriber = firebase.firestore().collection(Database.TABLE_USER_REGISTERED_CASES).doc(uid).onSnapshot(() => {
            this.updateUserRegisteredCases()
        })
    }

    handleBackButton = () => {
        if (this.state.isEditingCase) {
            this.resetState()
            return true
        } else {
            return false
        }
    }

    renderRegisteredCases = ({ item }) => {
        return (
            <TouchableOpacity activeOpacity={0.8}
                onPress={() => {
                    this.setState({
                        isEditingCase: true,
                        caseEditing: item,
                        name: item.name,
                        description: item.description,
                        birthDate: item.birthDate,
                        disappearanceDate: item.disappearanceDate,
                        city: item.city,
                        gender: item.gender,
                        imageURI: item.image,
                    })
                }}>
                <View style={GenericStyles.mainCasesViewContainerStyle}>
                    <Image source={{ uri: item.image }} style={GenericStyles.baseCaseImageOnListStyle} />
                    <View style={GenericStyles.casesInfosAndIconContainerStyle}>
                        <View style={GenericStyles.caseInfosContainerStyle}>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseNameStyle]}>{item.name}</Text>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{Utils.getCurrentAge(item.birthDate)} anos</Text>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{item.city}</Text>
                        </View>
                    </View>
                    {/* <Icon name="mode-edit" color={Colors.WHITE} size={25} style={{ marginRight: 20 }} /> */}
                </View>
            </TouchableOpacity>
        )
    }

    updateCaseAsync = async () => {
        try {
            this.setState({
                isProgressRingVisible: true
            })

            const downloadURI = await Storage.uploadPhotoToStorageAsync(this.state.imageURI, Enums.CurrentStorageFolder.IMAGES_FOLDER)

            this.setState({
                imageURI: downloadURI
            })

            Database.updateCaseInfos({
                name: this.state.name,
                description: this.state.description,
                birthDate: this.state.birthDate,
                disappearanceDate: this.state.disappearanceDate,
                city: this.state.city,
                gender: this.state.gender,
                id: this.state.caseEditing.id,
                image: downloadURI,
            })

            Utils.showFlashMessage(StringConstants.MISSING_APP_CASE_MODIFIED)

            await this.updateUserRegisteredCases()

            this.setState({
                isEditingCase: false,
                isProgressRingVisible: false,
            })
        } catch (err) {
            console.log("<<ERROR MODIFYING CASE >>")
            console.log(err)
            this.setState({
                isEditingCase: false,
                isProgressRingVisible: false,
            })
        }
    }

    updateUserRegisteredCases = async () => {
        let cases = await Database.getSavedOrRegisteredCasesAsync(Database.TABLE_USER_REGISTERED_CASES)
        this.setState({
            cases: cases,
        })

        this.resetState()
    }

    resetState() {
        this.setState({
            isEditingCase: false,
            caseEditing: [],
            name: '',
            description: '',
            birthDate: '',
            disappearanceDate: '',
            city: '',
            gender: '',
            imageURI: '',
        })
    }
    //#endregion

    //#region RENDER
    render() {
        if (!this.state.isEditingCase) {
            return (
                <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>
                    <BackIconHeader title={StringConstants.MISSING_APP_REGISTERED_CASES_TITLE}
                        onBackPress={() => this.props.navigation.goBack()}
                        icon={Enums.HeaderIcon.NONE} />

                    <FlatList data={this.state.cases}
                        renderItem={this.renderRegisteredCases}
                        keyExtractor={i => i.id}
                        style={GenericStyles.mainFlatListContainerStyle} />
                </SafeAreaView>
            )
        } else {
            return (
                <Modal transparent={false} visible={true} onRequestClose={this.handleBackButton}>
                    <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>
                        {this.state.isProgressRingVisible &&
                            <View style={GenericStyles.progressCircleStyle}>
                                <Progress.Circle color={Colors.BUTTON_COLOR} size={Utils.getScreenWidth() / 2} indeterminate={true} />
                            </View>
                        }
                        <BackIconHeader title={StringConstants.MISSING_APP_EDIT_CASE_TITLE}
                            onBackPress={() => this.setState({ isEditingCase: false })}
                            icon={Enums.HeaderIcon.DELETE}
                            caseId={this.state.caseEditing.id}
                            imageURI={this.state.caseEditing.image} />

                        {!this.state.isProgressRingVisible &&
                            <CaseModifier updateCase={this.updateCaseAsync}
                                caseInfos={{
                                    name: this.state.name,
                                    description: this.state.description,
                                    birthDate: this.state.birthDate,
                                    disappearanceDate: this.state.disappearanceDate,
                                    city: this.state.city,
                                    gender: this.state.gender,
                                    imageURI: this.state.imageURI,
                                }}
                                currentModifierCasePage={Enums.CurrentModifierCasePage.USER_REGISTERED_CASE} />
                        }
                    </SafeAreaView>
                </Modal>
            )
        }
    }
    //#endregion
}