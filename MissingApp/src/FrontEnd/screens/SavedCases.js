import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, BackHandler, SafeAreaView } from 'react-native'
import BackIconHeader from '../components/BackIconHeader'
import DetailedCase from '../components/DetailedCase'
import Icon from 'react-native-vector-icons/MaterialIcons'
import firebase from 'firebase'
import Utils from '../../Commons/Utils'
import Colors from '../styles/Colors'
import GenericStyles from '../styles/GenericStyles'
import Database from '../../BackEnd/db/Database'
import StringConstants from '../../Commons/StringConstants'
import Enums from '../../Commons/Enums'
import PageTokens from '../../Commons/PageTokens'

let unsubscriber;

export default class SavedCases extends Component {

    //#region CONSTRUCTOR, MOUNT AND UNMOUNT
    constructor(props) {
        super(props)
        this.state = {
            cases: [],
            isDetailedCase: false,
            detailedCase: [],
        }
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)

        let userUid = Utils.getCurrentUserFirebaseUid()
        unsubscriber = firebase.firestore().collection(Database.TABLE_SAVED_CASES).doc(userUid).onSnapshot(async () => {
            console.log("<< SNAPSHOT SAVED CASES -> SAVED CASES PAGE >>")
            await this.updateCurrentSavedCases()
        })

        try {
            await this.updateCurrentSavedCases()
        } catch (err) {
            console.log("<< ERROR FETCHING DATA FROM DATABASE -> SAVED CASES PAGE >>")
            console.log(err)
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
        unsubscriber()
    }
    //#endregion

    //#region METHODS
    updateCurrentSavedCases = async () => {
        let cases = await Database.getSavedOrRegisteredCasesAsync(Database.TABLE_SAVED_CASES)
        this.setState({
            cases: cases,
        })
        //console.log(cases)
    }

    handleBackButton = () => {
        if (this.state.isDetailedCase) {
            this.resetDetailedCase()
            return true
        } else {
            return false
        }
    }

    resetDetailedCase = () => {
        this.setState({ detailedCase: [], isDetailedCase: false })
    }

    renderSavedCases = ({ item }) => {
        return (
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ detailedCase: item, isDetailedCase: true })}>
                <View style={GenericStyles.mainCasesViewContainerStyle}>
                    <Image source={{ uri: item.image }} style={GenericStyles.baseCaseImageOnListStyle} />
                    <View style={GenericStyles.casesInfosAndIconContainerStyle}>
                        <View style={GenericStyles.caseInfosContainerStyle}>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseNameStyle]}>{item.name}</Text>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{Utils.getCurrentAge(item.birthDate)} anos</Text>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{item.city}</Text>
                        </View>
                        {/* <Icon name="bookmark" color={Colors.WHITE} size={25} onPress={async () => {
                            console.log("<< REMOVING SAVED CASE FROM DB >>")
                            await Database.removeCaseFromSavedCasesTableAsync(item.id)
                        }} /> */}
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    //#endregion

    //#region RENDER
    render() {
        if (!this.state.isDetailedCase) {
            return (
                <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>
                    <BackIconHeader title={StringConstants.MISSING_APP_SAVED_CASES_TITLE}
                        onBackPress={() => this.props.navigation.goBack()}
                        icon={Enums.HeaderIcon.NONE} />

                    <FlatList data={this.state.cases}
                        renderItem={this.renderSavedCases}
                        keyExtractor={i => i.id}
                        style={GenericStyles.mainFlatListContainerStyle} />

                </SafeAreaView>
            )
        } else {
            return (
                <DetailedCase item={this.state.detailedCase}
                    onBackPress={this.resetDetailedCase}
                    caseId={this.state.detailedCase.id}
                    isCaseSaved={true}
                    page={PageTokens.SAVED_CASES} />
            )
        }
    }
    //#endregion
}