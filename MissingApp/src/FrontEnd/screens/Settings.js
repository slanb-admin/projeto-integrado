import React, { Component } from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import BackIconHeader from '../components/BackIconHeader'
import { RadioButton } from 'react-native-paper'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import Colors from '../styles/Colors'
import Enums from '../../Commons/Enums'
import StringConstants from '../../Commons/StringConstants'

export default class Settings extends Component {

    //#region CONSTRUCTOR
    constructor(props) {
        super(props)
        this.state = {
            radioButtonNotificationValue: 'D',
            radioButtonThemeValue: 'D',
        }
    }
    //#endregion

    //#region RENDER
    render() {
        return (
            <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>
                <BackIconHeader title={StringConstants.MISSING_APP_SETTINGS_TITLE} onBackPress={() => this.props.navigation.goBack()} icon={Enums.HeaderIcon.NONE} />

                <View style={CustomStyles.settingsNotificationContainerStyle}>
                    <Icon name="notifications" color={Colors.WHITE} size={20} />
                    <Text style={CustomStyles.settingsTextStyle}>Notificações</Text>
                </View>

                <RadioButton.Group onValueChange={(value) => this.setState({ radioButtonNotificationValue: value })} value={this.state.radioButtonNotificationValue}>
                    <View style={CustomStyles.settingsRadioButtonContainerStyle}>
                        <Text style={CustomStyles.settingsRadioButtonTextStyle}>Habilitado</Text>
                        <RadioButton value="H" color={Colors.BUTTON_COLOR} uncheckedColor={Colors.BUTTON_COLOR} />
                    </View>
                    <View style={CustomStyles.settingsRadioButtonContainerStyle}>
                        <Text style={CustomStyles.settingsRadioButtonTextStyle}>Desabilitado</Text>
                        <RadioButton value="D" status="checked" uncheckedColor={Colors.BUTTON_COLOR} />
                    </View>
                </RadioButton.Group>

                <View style={CustomStyles.settingsThemeContainerStyle}>
                    <Icon name="palette" color={Colors.WHITE} size={20} />
                    <Text style={CustomStyles.settingsTextStyle}>Tema</Text>
                </View>

                <RadioButton.Group onValueChange={(value) => this.setState({ radioButtonThemeValue: value })} value={this.state.radioButtonThemeValue}>
                    <View style={CustomStyles.settingsRadioButtonContainerStyle}>
                        <Text style={CustomStyles.settingsRadioButtonTextStyle}>Claro</Text>
                        <RadioButton value="L" color={Colors.BUTTON_COLOR} uncheckedColor={Colors.BUTTON_COLOR} />
                    </View>
                    <View style={CustomStyles.settingsRadioButtonContainerStyle}>
                        <Text style={CustomStyles.settingsRadioButtonTextStyle}>Escuro</Text>
                        <RadioButton value="D" status="checked" uncheckedColor={Colors.BUTTON_COLOR} />
                    </View>
                </RadioButton.Group>
            </SafeAreaView>
        )
    }
    //#endregion
}