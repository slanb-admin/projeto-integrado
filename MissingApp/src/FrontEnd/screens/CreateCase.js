import React, { Component } from 'react'
import {
    View,
    SafeAreaView,
} from 'react-native'
import Database from '../../BackEnd/db/Database'
import GenericStyles from '../styles/GenericStyles'
import Utils from '../../Commons/Utils'
import Colors from '../styles/Colors'
import BackIconHeader from '../components/BackIconHeader'
import StringConstants from '../../Commons/StringConstants'
import Enums from '../../Commons/Enums'
import * as Progress from 'react-native-progress'
import GlobalState from '../../Commons/GlobalState'
import CaseModifier from '../components/CaseModifier'
import Storage from '../../BackEnd/storage/Storage'

export default class CreateCase extends Component {

    //#region CONSTRUCTOR
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            description: '',
            birthDate: '',
            disappearanceDate: '',
            city: '',
            gender: '',
            imageURI: '',
            isProgressRingVisible: false,
        }
        GlobalState.CREATE_CASE_SCREEN = this
    }
    //#endregion

    //#region METHODS
    registerCaseAsync = async () => {
        try {
            this.setState({
                isProgressRingVisible: true
            })

            const downloadURI = await Storage.uploadPhotoToStorageAsync(this.state.imageURI, Enums.CurrentStorageFolder.IMAGES_FOLDER)

            this.setState({
                imageURI: downloadURI
            })

            Database.registerCaseAsync({
                name: this.state.name,
                city: this.state.city,
                birthDate: this.state.birthDate,
                disappearanceDate: this.state.disappearanceDate,
                description: this.state.description,
                gender: this.state.gender,
                image: downloadURI,
            })

            Utils.showFlashMessage(StringConstants.MISSING_APP_CASE_CREATED)

            this.resetStates()

        } catch (err) {
            console.log("<< ERROR CREATING CASE >>")
            console.log(err)
            this.resetStates()
        }
    }

    resetStates() {
        this.setState({
            name: '',
            description: '',
            birthDate: '',
            disappearanceDate: '',
            city: '',
            gender: '',
            imageURI: '',
            isProgressRingVisible: false,
        })
    }
    //#endregion

    //#region RENDER
    render() {
        return (
            <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>
                {this.state.isProgressRingVisible &&
                    <View style={GenericStyles.progressCircleStyle}>
                        <Progress.Circle color={Colors.BUTTON_COLOR} size={Utils.getScreenWidth() / 2} indeterminate={true} />
                    </View>
                }
                <BackIconHeader title={StringConstants.MISSING_APP_CREATE_CASE_TITLE} onBackPress={() => this.props.navigation.goBack()} icon={Enums.HeaderIcon.NONE} />

                {!this.state.isProgressRingVisible &&
                    <CaseModifier updateCase={this.registerCaseAsync}
                        caseInfos={{
                            name: this.state.name,
                            description: this.state.description,
                            birthDate: this.state.birthDate,
                            disappearanceDate: this.state.disappearanceDate,
                            city: this.state.city,
                            gender: this.state.gender,
                            imageURI: this.state.imageURI,
                        }}
                        currentModifierCasePage={Enums.CurrentModifierCasePage.CREATE_CASE} />
                }
            </SafeAreaView>
        )
    }
    //#endregion
};