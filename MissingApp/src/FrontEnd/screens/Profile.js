import React, { Component } from 'react'
import { View, Text, TouchableOpacity, SafeAreaView, Image } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Header } from '../components/Header'
import Colors from '../styles/Colors'
import Utils from '../../Commons/Utils'
import firebase from 'firebase'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import PageTokens from '../../Commons/PageTokens'
import Database from '../../BackEnd/db/Database'

let unsubscriber;

export default class Profile extends Component {

    //#region CONSTRUCTOR, MOUNT AND UNMOUNT
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            userProfileImage: '',
        }
    }

    async componentDidMount() {
        let userUid = Utils.getCurrentUserFirebaseUid()
        unsubscriber = firebase.firestore().collection(Database.TABLE_PROFILE).doc(userUid).onSnapshot(async () => {
            await this.updateProfileInfosAsync()
        })
        await this.updateProfileInfosAsync()
    }

    componentWillUnmount() {
        unsubscriber()
    }
    //#endregion

    //#region METHODS
    updateProfileInfosAsync = async () => {
        let userNameAndProfileImage = await Database.getUserNameAndProfileImageAsync()

        this.setState({
            userProfileImage: userNameAndProfileImage[0],
            userName: userNameAndProfileImage[1],
        })
    }
    //#endregion

    render() {
        return (
            <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>
                <Header />

                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate(PageTokens.EDIT_PROFILE)}>
                    <View style={CustomStyles.profileImageContainerStyle}>
                        {this.state.userProfileImage === '' &&
                            <Icon name="account-circle" color={Colors.WHITE} size={Utils.getScreenWidth() / 3} style={{ borderRadius: 90 }} />
                        }
                        {this.state.userProfileImage !== '' &&
                            <Image source={{ uri: this.state.userProfileImage }} style={CustomStyles.profileUserImageStyle} />
                        }
                    </View>
                </TouchableOpacity>

                <Text style={[GenericStyles.baseTextStyle, CustomStyles.profileUserNameTextStyle]}>{this.state.userName}</Text>

                <View style={CustomStyles.profileButtonContainer}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate(PageTokens.CREATE_CASE)}>
                        <View style={[GenericStyles.baseButtonStyle, CustomStyles.profileMainTouchableContainerStyle]}>
                            <Icon name="add" size={15} color={Colors.BACKGROUND_COLOR} />
                            <Text style={CustomStyles.profileTextButtonStyle}>Cadastrar Caso</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate(PageTokens.USER_REGISTERED_CASES)}>
                        <View style={[GenericStyles.baseButtonStyle, CustomStyles.profileMainTouchableContainerStyle]}>
                            <Icon name="mode-edit" size={15} color={Colors.BACKGROUND_COLOR} />
                            <Text style={CustomStyles.profileTextButtonStyle}>Modificar Caso</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate(PageTokens.SAVED_CASES)}>
                        <View style={[GenericStyles.baseButtonStyle, CustomStyles.profileMainTouchableContainerStyle]}>
                            <Icon name="save-alt" size={15} color={Colors.BACKGROUND_COLOR} />
                            <Text style={CustomStyles.profileTextButtonStyle}>Casos Salvos</Text>
                        </View>
                    </TouchableOpacity>

                    {/* <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate(PageTokens.SETTINGS)}>
                        <View style={CustomStyles.profileMainTouchableContainerStyle}>
                            <Icon name="settings" size={15} color={Colors.BACKGROUND_COLOR} />
                            <Text style={CustomStyles.profileTextButtonStyle}>Configurações</Text>
                        </View>
                    </TouchableOpacity> */}

                    <TouchableOpacity activeOpacity={0.8} onPress={() => firebase.auth().signOut()}>
                        <View style={[GenericStyles.baseButtonStyle, CustomStyles.profileMainTouchableContainerStyle, { backgroundColor: Colors.LOGOUT_BUTTON_COLOR }]}>
                            <Icon name="logout" size={15} color={Colors.WHITE} />
                            <Text style={[CustomStyles.profileTextButtonStyle, { color: Colors.WHITE }]}>Sair</Text>
                        </View>
                    </TouchableOpacity>

                </View>

            </SafeAreaView>
        )
    }
};