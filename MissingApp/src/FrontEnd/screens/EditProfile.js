import React, { Component } from 'react'
import { SafeAreaView, View, Image, TextInput, TouchableOpacity, Text } from 'react-native'
import BackIconHeader from '../components/BackIconHeader'
import StringConstants from '../../Commons/StringConstants'
import Enums from '../../Commons/Enums'
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as ImagePicker from 'expo-image-picker'
import Utils from '../../Commons/Utils'
import Database from '../../BackEnd/db/Database'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import * as Progress from 'react-native-progress'
import Colors from '../styles/Colors'
import Storage from '../../BackEnd/storage/Storage'

export default class EditProfile extends Component {

    //#region CONSTRUCTOR AND MOUNT
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            userProfileImage: '',
            initialUserName: '',
            isProgressRingVisible: false,
            isModifyButtonDisabled: true,
            hasChangedImage: false,
        }
    }

    async componentDidMount() {
        await this.updateProfileInfosAsync()
    }
    //#endregion

    //#region METHODS
    updateProfileInfosAsync = async () => {
        let userNameAndProfileImage = await Database.getUserNameAndProfileImageAsync()

        this.setState({
            userProfileImage: userNameAndProfileImage[0],
            initialUserProfileImage: userNameAndProfileImage[0],
            userName: userNameAndProfileImage[1],
            initialUserName: userNameAndProfileImage[1],
        })
    }

    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            this.setState({
                userProfileImage: result.uri,
                hasChangedImage: true,
                isModifyButtonDisabled: false,
            })
        }
    }

    validateInfos = async () => {
        console.log('validateinfos')
        if (this.state.userName === '') {
            Utils.showFlashMessage(StringConstants.MISSING_APP_INVALID_USER_NAME)
        } else {
            await this.upateProfileAsync()
        }
    }

    async upateProfileAsync() {
        try {
            this.setState({
                isProgressRingVisible: true
            })

            let downloadURI = ''
            if (this.state.userProfileImage !== '') {
                downloadURI = await Storage.uploadPhotoToStorageAsync(this.state.userProfileImage, Enums.CurrentStorageFolder.PROFILE_FOLDER)
            }

            this.setState({
                userProfileImage: downloadURI
            })

            Database.updateProfileInfos({
                name: this.state.userName,
                image: downloadURI,
            })

            console.log("initial image url: " + this.state.initialUserProfileImage)
            if (this.state.hasChangedImage && this.state.initialUserProfileImage !== '') {
                await Storage.deleteImageFileFromStorage(this.state.initialUserProfileImage)
            }

            this.setState({
                isProgressRingVisible: false
            })

            Utils.showFlashMessage(StringConstants.MISSING_APP_PROFILE_UPDATED)
        } catch (err) {
            console.log("<< ERROR UPDATING PROFILE>>");
            console.log(err)
            this.setState({
                isProgressRingVisible: false
            })
        }
    }

    onChangeUserNameText = (value) => {
        if (value.length > 20) {
            return
        }

        let isModifyButtonDisabled = this.state.initialUserName !== value || this.state.hasChangedImage
        this.setState({
            userName: value,
            isModifyButtonDisabled: !isModifyButtonDisabled,
        })
    }
    //#endregion

    //#region RENDER
    render() {
        return (
            <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>

                {this.state.isProgressRingVisible &&
                    <View style={GenericStyles.progressCircleStyle}>
                        <Progress.Circle color={Colors.BUTTON_COLOR} size={Utils.getScreenWidth() / 2} indeterminate={true} />
                    </View>
                }

                <BackIconHeader title={StringConstants.MISSING_APP_EDIT_PROFILE_TITLE}
                    onBackPress={() => this.props.navigation.goBack()}
                    icon={Enums.HeaderIcon.NONE} />

                {!this.state.isProgressRingVisible &&
                    <View style={{ alignSelf: 'center' }}>

                        <Icon name="delete" color={Colors.WHITE} size={30}
                            style={{ marginBottom: -10, alignSelf: 'flex-end' }}
                            onPress={() => this.setState({ userProfileImage: '', hasChangedImage: true, isModifyButtonDisabled: false })} />

                        <View style={[CustomStyles.profileImageContainerStyle, { marginBottom: 10, alignItems: 'center' }]}>
                            {this.state.userProfileImage === '' &&
                                <Icon name="account-circle" color={Colors.WHITE} size={Utils.getScreenWidth() / 3} style={{ borderRadius: 90 }} />
                            }
                            {this.state.userProfileImage !== '' &&
                                <Image source={{ uri: this.state.userProfileImage }} style={CustomStyles.profileUserImageStyle} />
                            }
                        </View>

                        <View style={GenericStyles.baseInputStyle}>
                            <TextInput placeholder={StringConstants.MISSING_APP_USER_NAME_PLACEHOLDER}
                                placeholderTextColor={Colors.LIGHT_WHITE}
                                value={this.state.userName}
                                onChangeText={value => this.onChangeUserNameText(value)}
                                multiline={true}
                                style={[GenericStyles.baseTextStyle, CustomStyles.createCaseTextInputStyle]} />
                        </View>

                        <TouchableOpacity activeOpacity={0.8} onPress={this.pickImage}>
                            <View style={[GenericStyles.baseButtonStyle, { marginBottom: 10 }]}>
                                <Text style={GenericStyles.baseButtonTextStyle}>Trocar foto</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.8} onPress={this.validateInfos} disabled={this.state.isModifyButtonDisabled}>
                            <View style={[GenericStyles.baseButtonStyle, { opacity: this.state.isModifyButtonDisabled ? 0.7 : 1 }]}>
                                <Text style={GenericStyles.baseButtonTextStyle}>Modificar</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                }

            </SafeAreaView>
        )
    }
    //#endregion
}