import React, { Component } from 'react';
import { View, FlatList, Text, Image, BackHandler, SafeAreaView } from 'react-native'
import { Header } from '../components/Header'
import Utils from '../../Commons/Utils'
import { TouchableOpacity } from 'react-native'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import DetailedCase from '../components/DetailedCase'
import Database from '../../BackEnd/db/Database'
import GlobalState from '../../Commons/GlobalState'

export default class Home extends Component {

    //#region CONSTRUCTOR, MOUNT AND UNMOUNT
    constructor(props) {
        super(props)
        this.state = {
            cases: [],
            casesId: [],
            userSavedCases: [],
            isDetailedCase: false,
            detailedCase: [],
            detailedCaseIndex: -1,
        }

        GlobalState.HOME_SCREEN = this
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)

        try {
            let cases = await Database.getAllRegisteredCasesAsArray()
            let userSavedCases = await Database.getSavedOrRegisteredCasesIdAsync(Database.TABLE_SAVED_CASES)

            this.setState({
                cases: cases,
                userSavedCases: userSavedCases,
            })

        } catch (err) {
            console.log("<< ERROR FETCHING DATA FROM DATABSE -> HOME PAGE >>")
            console.log(err)
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
    }
    //#endregion

    //#region METHODS
    handleBackButton = () => {
        if (this.state.isDetailedCase) {
            this.setState({ detailedCase: [], isDetailedCase: false })
            return true
        } else {
            return false
        }
    }

    renderItems = ({ item }) => {
        return (
            <TouchableOpacity activeOpacity={0.9} onPress={() => this.setState({ detailedCase: item, isDetailedCase: true })}>
                <View style={CustomStyles.homeMainViewCasesStyle}>
                    <View style={CustomStyles.homeImageAndInfoContainerStyle}>
                        <Image source={{ uri: item.image }} style={CustomStyles.homeImageStyle} />
                        <View style={CustomStyles.homeTextContainerStyle}>
                            <Text style={[GenericStyles.baseTextStyle, CustomStyles.homePersonInfoDescriptionStyle]}>{item.name}</Text>
                            <Text style={[GenericStyles.baseTextStyle, CustomStyles.homePersonInfoDescriptionStyle]}>{Utils.getCurrentAge(item.birthDate)} anos</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    //#endregion

    //#region RENDER
    render() {
        if (!this.state.isDetailedCase) {
            return (
                <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>
                    <Header />
                    <Text style={[GenericStyles.baseTextStyle, CustomStyles.homeMoreInfoText]}>Para mais informações, clique na foto</Text>
                    <FlatList data={this.state.cases}
                        renderItem={this.renderItems}
                        keyExtractor={i => i.id} />
                </SafeAreaView>
            )
        } else {
            return (
                <DetailedCase item={this.state.detailedCase}
                    onBackPress={this.handleBackButton}
                    caseId={this.state.detailedCase.id}
                    isCaseSaved={this.state.userSavedCases.includes(this.state.detailedCase.id)} />
            )
        }
    }
    //#endregion
}

