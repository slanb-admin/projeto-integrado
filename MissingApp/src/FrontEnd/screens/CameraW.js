import React, { Component, createRef } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, BackHandler, FlatList } from 'react-native'
import { Camera } from 'expo-camera'
import { FontAwesome } from '@expo/vector-icons'
import { Image } from 'react-native'
import * as Permissions from 'expo-permissions'
import CustomStyles from '../styles/CustomStyles'
import GenericStyles from '../styles/GenericStyles'
import Colors from '../styles/Colors'
import { AntDesign } from '@expo/vector-icons'
import DetailedCase from '../components/DetailedCase'
import StringConstants from '../../Commons/StringConstants'
import * as Progress from 'react-native-progress'
import Utils from '../../Commons/Utils'
import axios from 'axios'
import Storage from '../../BackEnd/storage/Storage'
import Enums from '../../Commons/Enums'
import Database from '../../BackEnd/db/Database'
import BackIconHeader from '../components/BackIconHeader'

const PERMISSION_GRANTED = 'granted'
const SERVER_URL = 'http://18.228.199.209/compare'

export default class CameraW extends Component {

    //#region CONSTRUCTOR MOUNT AND UNMOUNT
    constructor(props) {
        super(props)
        this.state = {
            isCameraOpen: false,
            hasUserGivenPermission: true,
            capturedPhoto: '',
            type: Camera.Constants.Type.back,
            isCasePage: false,
            currentAICases: [],
            isDetailedCase: false,
            detailedCase: [],
            isProgressRingVisible: false,
        }
        this.cameraRef = createRef()
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)

        const { permisssion } = await Camera.requestPermissionsAsync();
        this.setState({
            hasUserGivenPermission: permisssion === PERMISSION_GRANTED
        })

        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasUserGivenPermission: status === PERMISSION_GRANTED
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
    }
    //#endregion

    //#region METHODS
    takePicture = async () => {
        if (this.cameraRef) {
            const data = await this.cameraRef.current.takePictureAsync();
            this.setState({
                capturedPhoto: data.uri,
                isCameraOpen: true,
            })
        }
    }

    //FIXME: send ai request
    sendPictureToAI = async () => {
        try {
            this.setState({
                isProgressRingVisible: true,
            })

            let downloadURI = await Storage.uploadPhotoToStorageAsync(this.state.capturedPhoto, Enums.CurrentStorageFolder.AI_FOLDER);
            var FormData = require('form-data')
            var data = new FormData();
            data.append('file', downloadURI)
            let aiResponse = await axios({
                method: 'post',
                url: SERVER_URL,
                data: data,
            })

            let cases = []
            cases = await Database.getCasesFromAIResponseAsync(aiResponse.data)

            Storage.deleteImageFileFromStorage(downloadURI)

            this.setState({
                isProgressRingVisible: false,
                currentAICases: cases,
            })

            if (cases.length > 0) {
                this.setState({
                    isCasePage: true,
                })
            } else {
                Utils.showFlashMessage(StringConstants.MISSING_APP_NO_AI_CASES_FOUND)
            }

        } catch (err) {
            this.setState({
                isProgressRingVisible: false,
                isCameraOpen: true,
                currentAICases: [],
            })
            console.log("<< CAMERA - Error while getting AI response >>")
            console.log(err);
            Utils.showFlashMessage(StringConstants.MISSING_APP_AI_ERROR_MESSAGE)
        }
    }

    renderAICases = ({ item }) => {
        return (
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ detailedCase: item, isDetailedCase: true, isCasePage: false, isCameraOpen: false, })}>
                <View style={GenericStyles.mainCasesViewContainerStyle}>
                    <Image source={{ uri: item.image }} style={GenericStyles.baseCaseImageOnListStyle} />
                    <View style={GenericStyles.casesInfosAndIconContainerStyle}>
                        <View style={GenericStyles.caseInfosContainerStyle}>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseNameStyle]}>{item.name}</Text>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{Utils.getCurrentAge(item.birthDate)} anos</Text>
                            <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{item.city}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    handleBackButton = () => {
        if (this.state.isCasePage) {
            resetCasePageState()
            return true
        } else if (this.state.isDetailedCase) {
            this.resetDetailedCaseState()
            this.setState({
                isCameraOpen: false,
                isCasePage: true,
            })
            return true
        } else {
            return false
        }
    }

    resetDetailedCaseState = () => {
        this.setState({ detailedCase: [], isDetailedCase: false, isCasePage: true })
    }

    resetCasePageState = () => {
        this.setState({
            isCameraOpen: false,
            isCasePage: false,
            currentAICases: [],
        })
    }
    //#endregion

    //#region RENDER
    render() {
        //#region PERMISSION NOT GRANTED
        if (this.state.hasUserGivenPermission === null) {
            return <View />
        }

        if (!this.state.hasUserGivenPermission) {
            return <Text>Acesso negado!!!</Text>
        }
        //#endregion

        //#region AI CASES
        if (this.state.isCasePage) {
            return (
                <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>
                    <BackIconHeader title={StringConstants.MISSING_APP_AI_CASES_TITLE}
                        onBackPress={this.resetCasePageState}
                        icon={Enums.HeaderIcon.NONE} />

                    <FlatList data={this.state.currentAICases}
                        renderItem={this.renderAICases}
                        keyExtractor={i => i.id}
                        style={GenericStyles.mainFlatListContainerStyle} />

                </SafeAreaView>
            )
        }

        if (this.state.isDetailedCase) {
            return (
                <DetailedCase item={this.state.detailedCase}
                    onBackPress={this.resetDetailedCaseState}
                    caseId={this.state.detailedCase.id}
                    isCaseSaved={true} />
            )
        }
        //#endregion

        //#region CAMERA
        if (!this.state.isCameraOpen) {
            return (
                <SafeAreaView style={CustomStyles.cameraContainerStyle}>
                    <Camera style={{ flex: 1 }} type={this.state.type} ref={this.cameraRef}>

                        <View style={CustomStyles.cameraChangeContainerStyle}>
                            <TouchableOpacity style={CustomStyles.cameraChangeIconStyle}
                                onPress={() => {
                                    this.setState({
                                        type: this.state.type === Camera.Constants.Type.back ? Camera.Constants.Type.front : Camera.Constants.Type.back
                                    })
                                }}>

                                <AntDesign name="sync" size={23} color={Colors.WHITE} />
                            </TouchableOpacity>
                        </View>

                    </Camera>

                    <TouchableOpacity style={CustomStyles.cameraTakePictureButtonStyle} onPress={this.takePicture}>
                        <FontAwesome name="camera" size={25} color={Colors.WHITE} />
                    </TouchableOpacity>
                </SafeAreaView>
            );
        } else {
            return (
                <View style={CustomStyles.pictureTakenContainerStyle}>

                    {this.state.isProgressRingVisible &&
                        <View style={GenericStyles.progressCircleStyle}>
                            <Progress.Circle color={Colors.BUTTON_COLOR} size={Utils.getScreenWidth() / 2} indeterminate={true} />
                        </View>
                    }

                    {!this.state.isProgressRingVisible &&
                        <View>
                            <Image style={CustomStyles.pictureTakenStyle} source={{ uri: this.state.capturedPhoto }} />

                            <View style={CustomStyles.pictureTakenButtonStyle}>

                                <TouchableOpacity style={{ marginRight: 20 }} onPress={() => this.setState({ isCameraOpen: false })}>
                                    <FontAwesome name="window-close" size={50} color={Colors.LOGOUT_BUTTON_COLOR} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={this.sendPictureToAI}>
                                    <FontAwesome name="upload" size={50} color={Colors.WHITE} />
                                </TouchableOpacity>

                            </View>
                        </View>
                    }
                </View>
            )
        }
        //#endregion
    }
    //#endregion
}