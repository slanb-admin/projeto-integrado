import React, { useState } from 'react'
import { View, Image, Text, TouchableOpacity, TextInput, SafeAreaView } from 'react-native'
import Colors from '../styles/Colors'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import firebase from '../../BackEnd/config/firebase'
import StringConstants from '../../Commons/StringConstants'
import Utils from '../../Commons/Utils'

export default Login = () => {

    //#region STATES
    const [areButtonsDisabled, setAreButtonsDisabled] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    //#endregion

    //#region REGISTER METHODS
    function validateSignInInfos() {
        if (email === '' || password === '') {
            Utils.showFlashMessage(StringConstants.MISSING_APP_FILL_ALL_FIELDS)
        } else if (isValidPassword()) {
            signInWithEmailAndPassword()
        }
    }

    function isValidPassword() {

        if (password.length < 5) {
            Utils.showFlashMessage(StringConstants.MISSING_APP_SMALL_PASSWORD)
            return false
        }

        if (!(/[a-z]/.test(password)) || !(/[A-Z]/.test(password)) || !(/\d/.test(password))) {
            Utils.showFlashMessage(StringConstants.MISSING_APP_INVALID_PASSWORD)
            return false
        }

        return true
    }

    function signInWithEmailAndPassword() {
        setAreButtonsDisabled(true)
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .catch(error => {
                setAreButtonsDisabled(false)
                console.log(error)

                if (error.code === 'auth/email-already-in-use') {
                    Utils.showFlashMessage(StringConstants.MISSING_APP_EMAIL_ALREADY_IN_USE)
                    console.log('That email address is already in use!');
                }

                if (error.code === 'auth/invalid-email') {
                    Utils.showFlashMessage(StringConstants.MISSING_APP_INVALID_EMAIL)
                    console.log('That email address is invalid!');
                }
            });
    }
    //#endregion

    return (
        <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>

            <View style={CustomStyles.registerImageContainerStyle}>
                <Image source={require('../../../assets/img/logo.png')} style={CustomStyles.registerImageStyle} />
                <Text style={CustomStyles.registerTextStyle}>Missing</Text>
            </View>

            <View style={GenericStyles.mainEmailAndPasswordContainerStyle}>
                <View style={GenericStyles.baseInputStyle}>
                    <TextInput placeholder={StringConstants.MISSING_APP_EMAIL_PLACEHOLDER}
                        placeholderTextColor={Colors.LIGHT_WHITE}
                        value={email}
                        style={[GenericStyles.baseTextStyle, CustomStyles.registerInputStyle]}
                        onChangeText={(value) => setEmail(value)} />
                </View>

                <View style={GenericStyles.baseInputStyle}>
                    <TextInput placeholder={StringConstants.MISSING_APP_PASSWORD_PLACEHOLDER}
                        placeholderTextColor={Colors.LIGHT_WHITE}
                        value={password}
                        secureTextEntry={true}
                        style={[GenericStyles.baseTextStyle, CustomStyles.registerInputStyle]}
                        onChangeText={(value) => setPassword(value)} />
                </View>

                <TouchableOpacity activeOpacity={0.8} onPress={validateSignInInfos} disabled={areButtonsDisabled}>
                    <View style={GenericStyles.baseButtonStyle}>
                        <Text style={GenericStyles.baseButtonTextStyle}>Cadastrar</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView >
    )
}