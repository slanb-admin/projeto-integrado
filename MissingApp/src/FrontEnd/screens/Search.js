import React, { Component, createRef } from 'react'
import { View, Image, Text, FlatList, SafeAreaView, TouchableOpacity, BackHandler, Modal } from 'react-native'
import { SearchBar } from 'react-native-elements'
import { Header } from '../components/Header'
import DetailedCase from '../components/DetailedCase'
import BackIconHeader from '../components/BackIconHeader'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import DatePicker from 'react-native-datepicker'
import SelectDropdown from 'react-native-select-dropdown'
import Colors from '../styles/Colors'
import Utils from '../../Commons/Utils'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import Enums from '../../Commons/Enums'
import Database from '../../BackEnd/db/Database'
import StringConstants from '../../Commons/StringConstants'
import GlobalState from '../../Commons/GlobalState'

export default class Search extends Component {

    //#region CONSTRUCTOR, MOUNT AND UNMOUNT
    constructor(props) {
        super(props)
        this.state = {
            cases: [],
            userSavedCases: [],
            filteredCases: [],
            searchValue: '',
            activePage: Enums.ActivePageEnum.SEARCH,
            genderFilter: '',
            cityFilter: '',
            birthDateFilter: '',
            sortCasesOrder: Enums.OrderEnum.NONE,
            detailedCase: [],
            previousGenderFilter: '',
            previousCityFilter: '',
            previousBirthDateFilter: '',
            previousSortCasesOrder: Enums.OrderEnum.NONE,
        }
        GlobalState.SEARCH_SCREEN = this;
        this.genderPickerRef = createRef()
        this.cityPickerRef = createRef()
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)

        try {
            let cases = await Database.getAllRegisteredCasesAsArray()
            let userSavedCases = await Database.getSavedOrRegisteredCasesIdAsync(Database.TABLE_SAVED_CASES)
            this.setState({
                cases: cases,
                userSavedCases: userSavedCases,
            })
        } catch (err) {
            console.log("<< ERROR FETCHING DATA FROM DATABSE -> SEARCH PAGE >>")
            console.log(err)
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
    }
    //#endregion

    //#region METHODS
    handleBackButton = () => {
        if (this.state.activePage == Enums.ActivePageEnum.FILTER || this.state.activePage == Enums.ActivePageEnum.DETAILED_CASE) {
            this.resetFilterState()
            return true
        } else {
            return false
        }
    }

    onChangeSearchText = (value) => {
        let filteredCases = []
        if (value !== '') {
            filteredCases = this.state.cases.filter(c => c.name.toUpperCase().includes(value.toUpperCase()))

            this.orderCases(filteredCases);

            if (this.state.genderFilter !== '') {
                filteredCases = filteredCases.filter(c => c.gender === this.state.genderFilter)
            }

            if (this.state.cityFilter !== '') {
                filteredCases = filteredCases.filter(c => c.city === this.state.cityFilter)
            }

            if (this.state.birthDateFilter !== '') {
                filteredCases = filteredCases.filter(c => c.birthDate === this.state.birthDateFilter)
            }
        }

        this.setState({
            filteredCases: filteredCases,
            searchValue: value,
        })
    }

    orderCases = (filteredCases) => {
        switch (this.state.sortCasesOrder) {
            case Enums.OrderEnum.NONE:
                filteredCases = filteredCases.sort((a, b) => a.name > b.name)
                break;
            case Enums.OrderEnum.LOWER_AGE:
                filteredCases = filteredCases.sort((a, b) => Utils.getCurrentAge(a.birthDate) > Utils.getCurrentAge(b.birthDate))
                break;
            case Enums.OrderEnum.HIGHER_AGE:
                filteredCases = filteredCases.sort((a, b) => Utils.getCurrentAge(a.birthDate) < Utils.getCurrentAge(b.birthDate))
                break;
            case Enums.OrderEnum.LOWER_DATE:
                filteredCases = filteredCases.sort((a, b) => a.disappearanceDate > b.disappearanceDate)
                break;
            case Enums.OrderEnum.HIGHER_DATE:
                filteredCases = filteredCases.sort((a, b) => a.disappearanceDate < b.disappearanceDate)
                break;
            default:
                break;
        }
    }

    renderFilteredCases = ({ item }) => {
        return (
            <TouchableOpacity activeOpacity={0.9} onPress={() => {
                this.setState({ detailedCase: item, activePage: Enums.ActivePageEnum.DETAILED_CASE })
            }}>
                <View style={GenericStyles.mainCasesViewContainerStyle}>
                    <Image source={{ uri: item.image }} style={GenericStyles.baseCaseImageOnListStyle} />
                    <View style={GenericStyles.caseInfosContainerStyle}>
                        <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseNameStyle]}>{item.name}</Text>
                        <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{Utils.getCurrentAge(item.birthDate)} anos</Text>
                        <Text style={[GenericStyles.baseTextStyle, GenericStyles.caseAgeAndCityStyle]}>{item.city}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    prepareStateToFilterPage = () => {
        this.setState({
            activePage: Enums.ActivePageEnum.FILTER,
            previousSortCasesOrder: this.state.sortCasesOrder,
            previousGenderFilter: this.state.genderFilter,
            previousCityFilter: this.state.cityFilter,
            previousBirthDateFilter: this.state.birthDateFilter,
        })
    }

    resetFilterState = () => {

        if (this.state.previousCityFilter === '') {
            this.cityPickerRef.current.reset()
        }

        if (this.state.previousGenderFilter === '') {
            this.genderPickerRef.current.reset()
        }

        this.setState({
            activePage: Enums.ActivePageEnum.SEARCH,
            detailedCase: [],
            sortCasesOrder: this.state.previousSortCasesOrder,
            genderFilter: this.state.previousGenderFilter,
            birthDateFilter: this.state.previousBirthDateFilter,
            cityFilter: this.state.previousCityFilter,
        })
    }
    //#endregion

    //#region RENDER
    render() {
        if (this.state.activePage == Enums.ActivePageEnum.SEARCH) {
            return (
                <SafeAreaView style={[GenericStyles.baseMainPageContainerStyle, GenericStyles.mainViewStyleIgnoringFooterHeight]}>
                    <Header />

                    <SearchBar inputStyle={CustomStyles.searchButtonSearchBarInputStyle}
                        inputContainerStyle={CustomStyles.searchButtonSearchBarContainerInputStyle}
                        containerStyle={CustomStyles.searchButtonSearchBarContainerStyle}
                        placeholder={StringConstants.MISSING_APP_SEARCH_PLACEHOLDER}
                        placeholderTextColor={Colors.LIGHT_WHITE}
                        onChangeText={this.onChangeSearchText}
                        value={this.state.searchValue}
                        onClear={() => this.setState({ filteredCases: [] })} />

                    <TouchableOpacity onPress={this.prepareStateToFilterPage} activeOpacity={0.5}>
                        <View style={CustomStyles.searchFilterButtonContainerStyle}>
                            <Ionicons name="filter" color={Colors.WHITE} size={20} />
                            <Text style={[GenericStyles.baseTextStyle, CustomStyles.searchFilterButtonTextStyle]}>Filtrar</Text>
                        </View>
                    </TouchableOpacity>

                    <FlatList data={this.state.filteredCases}
                        renderItem={this.renderFilteredCases}
                        keyExtractor={item => item.name}
                        style={[GenericStyles.mainFlatListContainerStyle, { marginTop: 0 }]} />

                </SafeAreaView>
            )
        } else if (this.state.activePage == Enums.ActivePageEnum.FILTER) {
            return (
                <Modal transparent={false} visible={true} onRequestClose={this.handleBackButton}>
                    <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>
                        <BackIconHeader title={StringConstants.MISSING_APP_FILTER_PLACEHOLDER}
                            onBackPress={this.resetFilterState}
                            icon={Enums.HeaderIcon.NONE} />

                        <Text style={[GenericStyles.baseTextStyle, CustomStyles.filterTextContainerStyle]}>Ordenar por:</Text>

                        <View style={CustomStyles.filterContainerOrderStyle}>

                            <View style={CustomStyles.filterButtonContainerStyle}>
                                <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ sortCasesOrder: Enums.OrderEnum.LOWER_AGE })}>
                                    <View style={[GenericStyles.baseButtonStyle, CustomStyles.filterButtonStyle,
                                    { opacity: this.state.sortCasesOrder === Enums.OrderEnum.LOWER_AGE || this.state.sortCasesOrder === Enums.OrderEnum.NONE ? 1 : 0.7 }]}>
                                        <Text style={[GenericStyles.baseButtonTextStyle, CustomStyles.filterTextButtonStyle]}>Menor idade</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ sortCasesOrder: Enums.OrderEnum.HIGHER_AGE })}>
                                    <View style={[GenericStyles.baseButtonStyle, CustomStyles.filterButtonStyle,
                                    { opacity: this.state.sortCasesOrder === Enums.OrderEnum.HIGHER_AGE || this.state.sortCasesOrder === Enums.OrderEnum.NONE ? 1 : 0.7 }]}>
                                        <Text style={[GenericStyles.baseButtonTextStyle, CustomStyles.filterTextButtonStyle]}>Maior idade</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={CustomStyles.filterButtonContainerStyle}>

                                <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ sortCasesOrder: OrderEnum.LOWER_DATE })}>
                                    <View style={[GenericStyles.baseButtonStyle, CustomStyles.filterButtonStyle,
                                    { opacity: this.state.sortCasesOrder === Enums.OrderEnum.LOWER_DATE || this.state.sortCasesOrder === Enums.OrderEnum.NONE ? 1 : 0.7 }]}>
                                        <Text style={[GenericStyles.baseButtonTextStyle, CustomStyles.filterTextButtonStyle]}>Menor data de desaparecimento</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ sortCasesOrder: Enums.OrderEnum.HIGHER_DATE })}>
                                    <View style={[GenericStyles.baseButtonStyle, CustomStyles.filterButtonStyle,
                                    { opacity: this.state.sortCasesOrder === Enums.OrderEnum.HIGHER_DATE || this.state.sortCasesOrder === Enums.OrderEnum.NONE ? 1 : 0.7 }]}>
                                        <Text style={[GenericStyles.baseButtonTextStyle, CustomStyles.filterTextButtonStyle]}>Maior data de desaparecimento</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={CustomStyles.filterContainerTextInput}>
                            <SelectDropdown
                                data={Utils.Genders}
                                defaultValueByIndex={Utils.GendersValues.indexOf(this.state.genderFilter)}
                                onSelect={(value) => this.setState({ genderFilter: value[0] })}
                                defaultButtonText="Sexo do desaparecido"
                                buttonStyle={[GenericStyles.baseInputStyle, GenericStyles.dropdownButtonStyle]}
                                buttonTextStyle={[GenericStyles.baseTextStyle, CustomStyles.dropdownButtonTextStyle,
                                { color: this.state.genderFilter === '' ? Colors.LIGHT_WHITE : Colors.WHITE }]}
                                dropdownIconPosition="right"
                                renderDropdownIcon={() => <MaterialIcons name="arrow-drop-down" color={Colors.BUTTON_COLOR} size={20} />}
                                rowTextStyle={GenericStyles.dropdownRowTextStyle}
                                ref={this.genderPickerRef}
                            />

                            <DatePicker
                                style={GenericStyles.datePickerStyle}
                                mode="date"
                                date={this.state.birthDateFilter}
                                format="DD/MM/YYYY"
                                minDate="01/01/1900"
                                maxDate="31/12/2099"
                                placeholder={StringConstants.MISSING_APP_BIRTH_DATE}
                                confirmBtnText={StringConstants.MISSING_APP_OK_STRING}
                                cancelBtnText={StringConstants.MISSING_APP_CANCEL_STRING}
                                showIcon={false}
                                onDateChange={(date) => this.setState({ birthDateFilter: date })}
                                customStyles={{
                                    dateInput: [GenericStyles.baseInputStyle, GenericStyles.datePickerInputStyle],
                                    dateText: [GenericStyles.baseTextStyle, CustomStyles.datePickerTextAndPlaceholderStyle],
                                    placeholderText: [GenericStyles.baseTextStyle, CustomStyles.datePickerTextAndPlaceholderStyle,
                                    { color: this.state.birthDateFilter === '' ? Colors.LIGHT_WHITE : Colors.WHITE }],
                                    dateTouchBody: GenericStyles.datePickerTouchBodyStyle,
                                }}
                            />

                            <SelectDropdown
                                data={Utils.Cities}
                                defaultValueByIndex={Utils.Cities.indexOf(this.state.cityFilter)}
                                onSelect={(value) => this.setState({ cityFilter: value })}
                                defaultButtonText="Cidade do desaparecimento"
                                buttonStyle={[GenericStyles.baseInputStyle, GenericStyles.dropdownButtonStyle]}
                                buttonTextStyle={[GenericStyles.baseTextStyle, CustomStyles.dropdownButtonTextStyle,
                                { color: this.state.cityFilter === '' ? Colors.LIGHT_WHITE : Colors.WHITE }]}
                                dropdownIconPosition="right"
                                renderDropdownIcon={() => <MaterialIcons name="arrow-drop-down" color={Colors.BUTTON_COLOR} size={20} />}
                                rowTextStyle={GenericStyles.dropdownRowTextStyle}
                                ref={this.cityPickerRef}
                            />

                        </View>


                        <View style={CustomStyles.filterButtonContainerStyle}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => {
                                this.setState({ activePage: Enums.ActivePageEnum.SEARCH });
                                if (this.state.searchValue !== '') {
                                    this.onChangeSearchText(this.state.searchValue);
                                }
                            }}>
                                <View style={[GenericStyles.baseButtonStyle, CustomStyles.filterButtonStyle]}>
                                    <Text style={[GenericStyles.baseButtonTextStyle, CustomStyles.filterTextButtonStyle]}>Aplicar</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={0.8} onPress={() => {
                                this.setState({
                                    genderFilter: '',
                                    cityFilter: '',
                                    birthDateFilter: '',
                                    sortCasesOrder: Enums.OrderEnum.NONE,
                                })
                                this.genderPickerRef.current.reset()
                                this.cityPickerRef.current.reset()
                            }}>
                                <View style={[GenericStyles.baseButtonStyle, CustomStyles.filterButtonStyle]}>
                                    <Text style={[GenericStyles.baseButtonTextStyle, CustomStyles.filterTextButtonStyle]}>Limpar</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                    </SafeAreaView>
                </Modal>
            )
        } else {
            return (
                <DetailedCase item={this.state.detailedCase}
                    onBackPress={() => this.setState({ activePage: Enums.ActivePageEnum.SEARCH, detailedCase: [] })}
                    caseId={this.state.detailedCase.id}
                    isCaseSaved={this.state.userSavedCases.includes(this.state.detailedCase.id)} />
            )
        }
    }
    //#endregion
};